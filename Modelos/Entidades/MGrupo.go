package Entidades

import (
	"github.com/jinzhu/gorm"
	"../../Modulos/General"
	"fmt"
	"errors"
)

func CrearGrupo(grupo Grupos, tx *gorm.DB) (*Grupos, error) {
	if err := tx.Create(&grupo).Error;
		err != nil {
		return nil, err
	}

	return &grupo, nil
}

func FindAllGrupos(limit, pagina int, consultas []Consulta) ([]*Grupos, error) {
	db := GetDB()
	db = db.Preload("Persona").Preload("Partido").Preload("Estructura")
	grupos := make([]*Grupos, 5)

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&grupos)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&grupos).Error; gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return grupos, nil

}

func CancelarGrupo(id string, tx *gorm.DB) (*Grupos, error) {
	if err := tx.Where(&Grupos{Id: id}).Update("vigente", false).Error;
		err != nil {
		fmt.Println("No se encuentra el grupo ", err)
		return nil, err
	}

	return nil, nil
}

func EliminarGrupo(grupo Grupos, tx *gorm.DB) error {
	if err := tx.Delete(&grupo).Error;
		err != nil {
		return err
	}
	return nil
}

func ObtenerGrupos() (*[]Grupos, error) {
	conn := GetDB()

	var grupos []Grupos
	if err := conn.Set("gorm:auto_preload", true).Find(&grupos).Error;
		err != nil {
		return nil, err
	}

	return &grupos, nil
}
