package Entidades

import (
	"github.com/jinzhu/gorm"
	"fmt"
	"../../Modulos/General"
	"errors"
	"time"
)

func FindAllCasillas(limit, pagina int, consultas []Consulta) ([]*Casillas, error) {
	db := GetDB()
	db = db.Preload("Direccion")
	casillas := make([]*Casillas, 5)

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&casillas)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&casillas).Error; gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	var votos []Votos
	db2 := GetDB()

	for i, _ := range casillas {

		if err := db2.Preload("Partido").Where(&Votos{SeccionId: casillas[i].SeccionId, CasillaId: casillas[i].Id}).Find(&votos).Error;
			err != nil {
			fmt.Println("Error: ", err)
			return nil, err
		}
		casillas[i].Votos = votos
	}

	return casillas, nil

}

func CrearCasilla(casilla Casillas, tx *gorm.DB) (*Casillas, error) {
	if err := tx.Create(&casilla).Error;
		err != nil {
		return nil, err
	}

	return &casilla, nil
}

func EliminarCasilla(casilla Casillas, tx *gorm.DB) (error) {
	if err := tx.Where("id = ? AND seccion_id = ?", casilla.Id, casilla.SeccionId).Delete(&Casillas{}).Error;
		err != nil {
		fmt.Println("Error: ", err)
		return err
	}

	return nil
}

func ActualizarCasilla(casilla Casillas, tx *gorm.DB) (*Casillas, error) {
	if err := tx.Model(&casilla).Select("votos_totales", "votos_buenos", "votos_nulos",
		"hora_apertura", "hora_cierre", "observaciones", "incidencias").
		Updates(map[string]interface{}{"votos_totales": casilla.VotosTotales, "votos_buenos": casilla.VotosBuenos,
		"votos_nulos": casilla.VotosNulos, "hora_apertura": casilla.HoraApertura, "hora_cierre": casilla.HoraCierre,
		"observaciones": casilla.Observaciones}).Error;
		err != nil {
		return nil, err
	}

	return &casilla, nil
}

func HoraCasilla(casilla CasillaHora, tx *gorm.DB) (error) {

	if casilla.Apertura {
		if err := tx.Table("casillas").Where("id = ? AND seccion_id = ?",
			casilla.CasillaId, casilla.SeccionId).UpdateColumns(Casillas{HoraApertura: &casilla.Hora}).Error;
			err != nil {
			return err
		}
		return nil
	} else {
		if err := tx.Table("casillas").Where("id = ? AND seccion_id = ?",
			casilla.CasillaId, casilla.SeccionId).UpdateColumns(Casillas{HoraCierre: &casilla.Hora}).Error;
			err != nil {
			return err
		}
		return nil
	}

}

func ActualizarVotosCasilla(casilla, seccion string, total, nulos int, tx *gorm.DB) (error) {
	buenos := total - nulos

	var c1 Casillas

	if err := tx.Where("id = ? AND seccion_id = ?",
		casilla, seccion).First(&c1).Error;
		err != nil {
		return errors.New("Problema al actualizar la casilla")
	}

	if c1.Vigente {
		if err := tx.Table("casillas").Where("id = ? AND seccion_id = ?",casilla,seccion).UpdateColumns(Casillas{VotosTotales: &total, VotosNulos: &nulos, VotosBuenos: &buenos}).Error;
			err != nil {
			return errors.New("Problema al registrar los votos")
		}
		return nil
	} else {
		return errors.New("La casilla debe estar vigente para poder registrar")
	}
	return errors.New("Hubo un problema")
}

func DeshabilitarCasilla(casilla Casillas, perfil int, tx *gorm.DB) (error) {
	if !casilla.Vigente {
		if err := tx.Model(&casilla).Update("vigente",false).Error;
			err != nil {
			return err
		}

		return nil
	} else if casilla.Vigente && perfil == 2 || casilla.Vigente && perfil == 1 {
		if err := tx.Model(&casilla).Update("vigente",true).Error;
			err != nil {
			return err
		}
		return nil
	}

	return errors.New("Contacta a un admnistrador para habilitar la casilla")
}

func RegistrarVotosEstimados(votos VotosEstimados, tx *gorm.DB) (error) {
	if err := tx.Table("casillas").Where("id = ? AND seccion_id = ?",
		votos.CasillaId, votos.SeccionId).UpdateColumns(Casillas{VotosEstimados: &votos.VotosEstimados}).Error;
		err != nil {
		return err
	}
	return nil

}

func CancelarCasilla(id string, tx *gorm.DB) (*Casillas, error) {
	var casilla Casillas
	if err := tx.Where(&Casillas{Id: id}).First(&casilla).Error;
		err != nil {
		fmt.Println("No se encuentra la casilla ", err)
		return nil, err
	}

	casilla.Vigente = false

	if err := tx.Save(&casilla).Error;
		err != nil {
		return nil, errors.New("Error al eliminar casilla")
	}
	return &casilla, nil
}

func RegistrarVotos(votos Votos, tx *gorm.DB) (*Votos, error) {
	var voto Votos
	if err := tx.Where(&Votos{SeccionId: votos.SeccionId, CasillaId: votos.CasillaId, PartidoId: votos.PartidoId}).
		First(&voto).Error;
		gorm.IsRecordNotFoundError(err) {
		if err := tx.Create(&votos).Error;
			err != nil {
			return nil, err
		}
		return &votos, nil

	} else if err != nil {
		fmt.Println("Hubo un error :", err)
		return nil, err
	}

	//Se actualizan los votos en caso de que ya haya sido registrado anteriormente
	voto.NumeroVotos = votos.NumeroVotos

	if err := tx.Save(&voto).Error;
		err != nil {
		return nil, err
	}
	return &voto, nil
}

func RegistrarVotos2(votos Votos2, tx *gorm.DB) (*Votos2, error) {
	var voto Votos2
	if err := tx.Where(&Votos2{SeccionId: votos.SeccionId, CasillaId: votos.CasillaId, PartidoId: votos.PartidoId}).
		First(&voto).Error;
		gorm.IsRecordNotFoundError(err) {
		if err := tx.Create(&votos).Error;
			err != nil {
			return nil, err
		}
		return &votos, nil

	} else if err != nil {
		fmt.Println("Hubo un error :", err)
		return nil, err
	}
	//Se actualizan los votos en caso de que ya haya sido registrado anteriormente
	voto.NumeroVotos = votos.NumeroVotos

	if err := tx.Save(&voto).Error;
		err != nil {
		return nil, err
	}
	return &voto, nil
}

func FindIncidencias() (*[]Incidencias, error) {
	var incidencias []Incidencias
	conn := GetDB()
	if err := conn.Find(&incidencias).Error;
		err != nil {
		return nil, err
	}
	return &incidencias, nil
}

func RegistrarIncidencias(incidencias []CasillaIncidencia, tx *gorm.DB) (*[]CasillaIncidencia, error) {
	for _, inc := range incidencias {
		if err := tx.Create(&inc).Error;
			err != nil {
			return nil, err
		}

	}
	return &incidencias, nil
}

func EliminarIncidencia(incidencia CasillaIncidencia, tx *gorm.DB) (error) {
	if err := tx.Exec("DELETE from casilla_incidencia WHERE casilla_id = ? AND seccion_id = ? AND incidencia_id = ?", incidencia.CasillaId, incidencia.SeccionId, incidencia.IncidenciaId).Error;
		err != nil {
		return err
	}

	return nil
}

func FindAllIncidenciasCasilla(limit, pagina int, consultas []Consulta) ([]*CasillaIncidencia, error) {
	db := GetDB()
	db = db.Preload("Incidencia")
	incidencias := make([]*CasillaIncidencia, 5)

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&incidencias)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&incidencias).Error; gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return incidencias, nil

}

func RegistrarDireccionCasilla(casilla CasillaCambioDireccion, tx *gorm.DB) (error) {
	var c1 Casillas
	if err := tx.Where("seccion_id = ? AND id = ?", casilla.SeccionId, casilla.CasillaId).
		First(&c1).Error;
		err != nil {
		return err
	}

	var casDir CasillaDireccion
	casDir.CasillaId = casilla.CasillaId
	casDir.SeccionId = casilla.SeccionId
	casDir.Observacion = casilla.Observacion
	casDir.DireccionId = c1.DireccionId
	casDir.Hora = time.Now()
	var dir Direcciones
	dir = casilla.Direccion
	if err := tx.Create(&dir).Error;
		err != nil {
		return err
	}

	if err := tx.Model(&c1).UpdateColumn("direccion_id", dir.Id).Error;
		err != nil {
		return err
	}

	if err := tx.Create(&casDir).Error;
		err != nil {
		return err
	}

	return nil
}
