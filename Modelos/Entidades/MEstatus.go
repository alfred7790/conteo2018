package Entidades

import "errors"

func CrearActividad(actividades Actividades) (*Actividades, error) {
	conn := GetDB()
	if err := conn.Create(&actividades).Error;
		err != nil {
		return nil, errors.New("Error al crear actividad")
	}
	return &actividades, nil
}

func CrearTipoCoordinador(coor TipoCoordinador) (*TipoCoordinador, error) {
	conn := GetDB()
	if err := conn.Create(&coor).Error;
		err != nil {
		return nil, errors.New("Error al crear rol")
	}
	return &coor, nil
}
