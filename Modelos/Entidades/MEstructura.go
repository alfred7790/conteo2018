package Entidades

import "github.com/jinzhu/gorm"

func RegistrarEstructura(estructura Estructuras, tx *gorm.DB) (*Estructuras, error) {
	if err := tx.Create(&estructura).Error;
		err != nil {
		return nil, err
	}

	return &estructura, nil
}

func ObtenerEstructuras() (*[]Estructuras, error) {
	conn := GetDB()

	var estructura []Estructuras
	if err := conn.Find(&estructura).Error;
		err != nil {
		return nil, err
	}

	return &estructura, nil
}
