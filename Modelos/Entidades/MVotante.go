package Entidades

import (
	"../../Modulos/General"
	"github.com/jinzhu/gorm"
	"fmt"
	"errors"
)

func RegistrarPromovido(votante Votantes, tx *gorm.DB) (*Votantes, error) {
	if err := tx.Create(&votante).Error;
		err != nil {
		return nil, err
	}

	return &votante, nil
}

func EliminarPromovido(votante Votantes, tx *gorm.DB) (error) {
	if err := tx.Delete(&Votantes{}, "personapromotor_id = ? AND personavotante_id = ?",
		votante.PersonapromotorId, votante.PersonavotanteId).Error;
		err != nil {
		return err
	}

	return nil
}

func FindListaPromovidos(limit, pagina int, consultas []Consulta) (*[]ListaPromovidos, error) {
	db := GetDB()
	var personas []ListaPromovidos

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&personas)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&personas).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return &personas, nil

}
