package Entidades

import (
	"github.com/jinzhu/gorm"
	"fmt"
	"../../Modulos/General"
	"errors"
)

func CrearRepresentante(representante Representantes, tx *gorm.DB) (*Representantes, error) {
	if err := tx.Create(&representante).Error;
		err != nil {
		return nil, err
	}

	return &representante, nil
}

func EliminarRepresentanteCasilla(representante Representantes, tx *gorm.DB) (error) {
	if err := tx.Delete(&representante).Error;
		err != nil {
		fmt.Println("Error: ", err)
		return err
	}

	return nil
}

func FindRepresentantes(limit, pagina int, consultas []Consulta) (*[]Representantes, error) {
	db := GetDB()
	db = db.Set("gorm:auto_preload", true)

	var personas []Representantes

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&personas)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&personas).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return &personas, nil

}