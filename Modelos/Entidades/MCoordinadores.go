package Entidades

import (
	"github.com/jinzhu/gorm"
	"fmt"
	"errors"
	"../../Modulos/General"
)

func RegistrarCoordinadorZona(coordinador CoordinadorZonas, tx *gorm.DB) (*CoordinadorZonas, error) {
	if err := tx.Create(&coordinador).Error;
		err != nil {
		return nil, err
	}

	return &coordinador, nil
}

func EliminarCoordinadorZona(coordinador CoordinadorZonas, tx *gorm.DB) (error) {
	if err := tx.Where("personazona_id = ? AND grupo_id = ?", coordinador.PersonazonaId, coordinador.GrupoId).
		Delete(&CoordinadorZonas{}).Error;
		err != nil {
		fmt.Println("Error: ", err)
		return err
	}

	return nil
}

func RegistrarCoordinadorSeccion(coordinador CoordinadorSecciones, tx *gorm.DB) (*CoordinadorSecciones, error) {
	if err := tx.Create(&coordinador).Error;
		err != nil {
		return nil, err
	}

	return &coordinador, nil
}

func EliminarCoordinadorSeccion(coordinador CoordinadorSecciones, tx *gorm.DB) (error) {
	if err := tx.Where("personaseccion_id = ? AND grupo_id = ?",
		coordinador.PersonaseccionId, coordinador.GrupoId).Delete(&CoordinadorSecciones{}).Error;
		err != nil {
		return err
	}

	return nil
}

func RegistrarCoordinadorManzana(coordinador CoordinadorManzanas, tx *gorm.DB) (*CoordinadorManzanas, error) {
	if err := tx.Create(&coordinador).Error;
		err != nil {
		return nil, err
	}

	return &coordinador, nil
}

func EliminarCoordinadorManzana(coordinador CoordinadorManzanas, tx *gorm.DB) (error) {
	if err := tx.Where("personamanzana_id = ?",
		coordinador.PersonamanzanaId).Delete(&CoordinadorManzanas{}).Error;
		err != nil {
			fmt.Println(err.Error())
		return err
	}

	return nil
}

func RegistrarPromotor(promotor Promotores, tx *gorm.DB) (*Promotores, error) {
	if err := tx.Create(&promotor).Error;
		err != nil {
		return nil, err
	}

	return &promotor, nil
}

func EliminarPromotor(promotor Promotores, tx *gorm.DB) (error) {
	if err := tx.Where("personapromotor_id = ?",promotor.PersonapromotorId).Delete(&Promotores{}).Error;
		err != nil {
		return err
	}

	return nil
}

func BuscarEnCoordinadoresZona(persona string) (error) {
	conn := GetDB()

	var coordinador CoordinadorZonas
	if err := conn.Where("personazona_id = ?", persona).First(&coordinador).Error;
		gorm.IsRecordNotFoundError(err) {
		return nil
	} else if err != nil {
		return errors.New("Hubo un error en la busqueda")
	}
	return errors.New("Esta persona ah sido asignada a un grupo y no es posible eliminar su rol")
}

func BuscarEnCoordinadoresSeccion(persona string) (error) {
	conn := GetDB()

	var coordinador CoordinadorSecciones
	if err := conn.Where("personaseccion_id = ?", persona).First(&coordinador).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("No encontro registros, por lo tanto se puede eliminar")
		return nil
	} else if err != nil {
		fmt.Println("Hubo un error :", err)
		return errors.New("Hubo un error en la busqueda")
	}
	return errors.New("No se puede eliminar, esta asignado a una seccion")
}

func BuscarEnCoordinadoresManzana(persona string) (error) {
	conn := GetDB()

	var coordinador CoordinadorManzanas
	if err := conn.Where("personamanzana_id = ?", persona).First(&coordinador).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("No encontro registros, por lo tanto se puede eliminar")
		return nil
	} else if err != nil {
		fmt.Println("Hubo un error :", err)
		return errors.New("Hubo un error en la busqueda")
	}
	return errors.New("No se puede eliminar, esta asignado a una coordinador de seccion")
}

func BuscarEnPromotores(persona string) (error) {
	conn := GetDB()

	var promotor Promotores
	if err := conn.Where("personapromotor_id = ?", persona).First(&promotor).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("No encontro registros, por lo tanto se puede eliminar")
		return nil
	} else if err != nil {
		fmt.Println("Hubo un error :", err)
		return errors.New("Hubo un error en la busqueda")
	}
	return errors.New("No se puede eliminar, esta asignado a una coordinador de manzana")
}

func BuscarEnGrupos(persona string) (error) {
	conn := GetDB()

	var grupo Grupos
	if err := conn.Where("persona_id = ?", persona).First(&grupo).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("No encontro registros, por lo tanto se puede eliminar")
		return nil
	} else if err != nil {
		fmt.Println("Hubo un error :", err)
		return errors.New("Hubo un error en la busqueda")
	}
	return errors.New("No se puede eliminar, esta asignado a una coordinador de manzana")
}


func FindAllCoordinadoresZona(limit, pagina int, consultas []Consulta) ([]*CoordinadorZonas, error) {
	db := GetDB()
	db = db.Preload("Personazona")
	zona := make([]*CoordinadorZonas, 5)

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&zona)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&zona).Error; gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return zona, nil

}

func FindAllCoordinadoresSeccion(limit, pagina int, consultas []Consulta) ([]*CoordinadorSecciones, error) {
	db := GetDB()
	db = db.Preload("Personaseccion")
	seccion := make([]*CoordinadorSecciones, 5)

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&seccion)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&seccion).Error; gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return seccion, nil

}

func ObtenerRoles() (*[]TipoCoordinador, error) {
	db := GetDB()
	var tipos []TipoCoordinador
	if err := db.Find(&tipos).Error;
		err != nil {
		return nil, err
	}

	return &tipos, nil
}
