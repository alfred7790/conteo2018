package Entidades

func CrearTablas() {
	var db = GetDB()

	if !db.HasTable(&Secciones{}) {
		db.AutoMigrate(&Secciones{})
	}

	if !db.HasTable(&Roles{}) {
		db.AutoMigrate(&Roles{})
		CrearRol(Roles{Id: 1, Tipo: "Master"})
		CrearRol(Roles{Id: 1, Tipo: "Capturista"})
		CrearRol(Roles{Id: 1, Tipo: "Administrador"})
		CrearRol(Roles{Id: 1, Tipo: "AdministradorD"})

	}

	if !db.HasTable(&Actividades{}) {
		db.AutoMigrate(&Actividades{})
		CrearActividad(Actividades{Id: 1, Descripcion: ""})
		CrearActividad(Actividades{Id: 2, Descripcion: ""})
		CrearActividad(Actividades{Id: 3, Descripcion: ""})
		CrearActividad(Actividades{Id: 4, Descripcion: ""})
		CrearActividad(Actividades{Id: 5, Descripcion: ""})
		CrearActividad(Actividades{Id: 6, Descripcion: ""})
		CrearActividad(Actividades{Id: 7, Descripcion: ""})
		CrearActividad(Actividades{Id: 8, Descripcion: ""})
		CrearActividad(Actividades{Id: 9, Descripcion: ""})
		CrearActividad(Actividades{Id: 10, Descripcion: ""})
		CrearActividad(Actividades{Id: 11, Descripcion: ""})
		CrearActividad(Actividades{Id: 12, Descripcion: ""})
		CrearActividad(Actividades{Id: 13, Descripcion: ""})
		CrearActividad(Actividades{Id: 14, Descripcion: ""})
		CrearActividad(Actividades{Id: 15, Descripcion: ""})
		CrearActividad(Actividades{Id: 16, Descripcion: ""})
		CrearActividad(Actividades{Id: 17, Descripcion: ""})
		CrearActividad(Actividades{Id: 18, Descripcion: ""})
		CrearActividad(Actividades{Id: 19, Descripcion: ""})
		CrearActividad(Actividades{Id: 20, Descripcion: ""})
		CrearActividad(Actividades{Id: 21, Descripcion: ""})
		CrearActividad(Actividades{Id: 22, Descripcion: ""})
		CrearActividad(Actividades{Id: 23, Descripcion: ""})
		CrearActividad(Actividades{Id: 24, Descripcion: ""})
		CrearActividad(Actividades{Id: 25, Descripcion: ""})
		CrearActividad(Actividades{Id: 26, Descripcion: ""})
		CrearActividad(Actividades{Id: 27, Descripcion: ""})
		CrearActividad(Actividades{Id: 28, Descripcion: ""})
		CrearActividad(Actividades{Id: 29, Descripcion: ""})
		CrearActividad(Actividades{Id: 30, Descripcion: ""})
		CrearActividad(Actividades{Id: 31, Descripcion: ""})
		CrearActividad(Actividades{Id: 32, Descripcion: ""})
		CrearActividad(Actividades{Id: 33, Descripcion: ""})
		CrearActividad(Actividades{Id: 34, Descripcion: ""})
		CrearActividad(Actividades{Id: 35, Descripcion: ""})
		CrearActividad(Actividades{Id: 36, Descripcion: ""})
		CrearActividad(Actividades{Id: 37, Descripcion: ""})
		CrearActividad(Actividades{Id: 38, Descripcion: ""})
		CrearActividad(Actividades{Id: 39, Descripcion: ""})

	}

	if !db.HasTable(&Incidencias{}) {
		db.AutoMigrate(&Incidencias{})
	}

	if !db.HasTable(&FuncionariosHistorial{}) {
		db.AutoMigrate(&FuncionariosHistorial{})
	}

	if !db.HasTable(&CasillaDireccion{}) {
		db.AutoMigrate(&CasillaDireccion{})
	}

	if !db.HasTable(&Estructuras{}) {
		db.AutoMigrate(&Estructuras{})
	}

	if !db.HasTable(&Grupos{}) {
		db.AutoMigrate(&Grupos{}).
			AddForeignKey("partido_id", "partidos(id)", "RESTRICT", "CASCADE").
			AddForeignKey("persona_id", "personas(id)", "RESTRICT", "CASCADE").
			AddForeignKey("estructura_id", "estructuras(id)", "RESTRICT", "CASCADE")

	}

	if !db.HasTable(&Personas{}) {
		db.AutoMigrate(&Personas{}).
			AddForeignKey("seccion_id", "secciones(id)", "RESTRICT", "CASCADE")
	}

	if !db.HasTable(&TipoCoordinador{}) {
		db.AutoMigrate(&TipoCoordinador{})
	}

	if !db.HasTable(&Direcciones{}) {
		db.AutoMigrate(&Direcciones{}).
			AddForeignKey("persona_id", "personas(id)", "RESTRICT", "CASCADE")
	}

	if !db.HasTable(&Partidos{}) {
		db.AutoMigrate(&Partidos{}).
			AddForeignKey("persona_id", "personas(id)", "RESTRICT", "CASCADE")
	}

	if !db.HasTable(&Partidos2{}) {
		db.AutoMigrate(&Partidos2{}).
			AddForeignKey("persona_id", "personas(id)", "RESTRICT", "CASCADE")
	}

	if !db.HasTable(&PersonaCoordinador{}) {
		db.AutoMigrate(&PersonaCoordinador{}).
			AddForeignKey("persona_id", "personas(id)", "RESTRICT", "CASCADE").
			AddForeignKey("tipo_coordinador_id", "tipo_coordinador(id)", "RESTRICT", "CASCADE").
			AddForeignKey("partido_id", "partidos(id)", "RESTRICT", "CASCADE")

	}

	if !db.HasTable(&PersonaRepresentante{}) {
		db.AutoMigrate(&PersonaRepresentante{}).
			AddForeignKey("persona_id", "personas(id)", "RESTRICT", "CASCADE").
			AddForeignKey("tipo_representante_id", "tipo_coordinador(id)", "RESTRICT", "CASCADE")
	}

	if !db.HasTable(&Usuarios{}) {
		db.AutoMigrate(&Usuarios{}).
			AddForeignKey("rol_id", "roles(id)", "RESTRICT", "CASCADE")
	}

	if !db.HasTable(&Movimientos{}) {
		db.AutoMigrate(&Movimientos{}).AddForeignKey("actividad_id", "actividades(id)", "RESTRICT", "CASCADE").
			AddForeignKey("usuario_id", "usuarios(id)", "RESTRICT", "CASCADE")
	}

	if !db.HasTable(&Telefonos{}) {
		db.AutoMigrate(&Telefonos{}).AddForeignKey("persona_id", "personas(id)", "RESTRICT", "CASCADE")
	}

	if !db.HasTable(&Casillas{}) {
		db.AutoMigrate(&Casillas{}).AddForeignKey("direccion_id", "direcciones(id)", "RESTRICT", "CASCADE").
			AddForeignKey("seccion_id", "secciones(id)", "RESTRICT", "CASCADE").
			AddUniqueIndex("index_casilla", "id", "seccion_id")
	}

	if !db.HasTable(&Representantes{}) {
		db.AutoMigrate(&Representantes{}).AddForeignKey("partido_id", "partidos(id)", "RESTRICT", "CASCADE").
			AddForeignKey("persona_id", "personas(id)", "RESTRICT", "CASCADE").
			AddForeignKey("casilla_id,seccion_id", "casillas(id,seccion_id)",
			"RESTRICT", "CASCADE")
	}

	if !db.HasTable(&Funcionarios{}) {
		db.AutoMigrate(&Funcionarios{}).
			AddForeignKey("persona_id", "personas(id)", "RESTRICT", "CASCADE")

		db.Model(&Funcionarios{}).AddForeignKey("casilla_id,seccion_id", "casillas(id,seccion_id)",
			"RESTRICT", "CASCADE").
			AddUniqueIndex("index_funcionario", "persona_id", "casilla_id", "seccion_id")
	}

	if !db.HasTable(&Votos{}) {
		db.AutoMigrate(&Votos{}).
			AddForeignKey("partido_id", "partidos(id)", "RESTRICT", "CASCADE").
			AddForeignKey("casilla_id,seccion_id", "casillas(id,seccion_id)",
			"RESTRICT", "CASCADE").
			AddUniqueIndex("index_votos", "partido_id", "casilla_id", "seccion_id")
	}

	if !db.HasTable(&Votos2{}) {
		db.AutoMigrate(&Votos2{}).
			AddForeignKey("partido_id", "partidos(id)", "RESTRICT", "CASCADE").
			AddForeignKey("casilla_id,seccion_id", "casillas(id,seccion_id)",
			"RESTRICT", "CASCADE").
			AddUniqueIndex("index_votos", "partido_id", "casilla_id", "seccion_id")
	}

	if !db.HasTable(&CoordinadorZonas{}) {
		db.AutoMigrate(&CoordinadorZonas{}).
			AddForeignKey("personazona_id", "personas(id)", "RESTRICT", "CASCADE").
			AddForeignKey("grupo_id", "grupos(id)", "RESTRICT", "CASCADE").
			AddUniqueIndex("index_zona", "personazona_id", "grupo_id")

	}

	if !db.HasTable(&CoordinadorSecciones{}) {
		db.AutoMigrate(&CoordinadorSecciones{}).
			AddForeignKey("grupo_id", "grupos(id)", "RESTRICT", "CASCADE").
			AddForeignKey("seccion_id", "secciones(id)", "RESTRICT", "CASCADE").
			AddForeignKey("personaseccion_id", "personas(id)", "RESTRICT", "CASCADE").
			AddUniqueIndex("index_seccion", "grupo_id", "seccion_id", "personaseccion_id")

	}

	if !db.HasTable(&CoordinadorManzanas{}) {
		db.AutoMigrate(&CoordinadorManzanas{}).
			AddForeignKey("personamanzana_id", "personas(id)", "RESTRICT", "CASCADE")

		db.Model(&CoordinadorManzanas{}).AddForeignKey("grupo_id,seccion_id,personaseccion_id",
			"coordinador_secciones(grupo_id,seccion_id,personaseccion_id)",
			"RESTRICT", "CASCADE").
			AddUniqueIndex("index_manzana",
			"grupo_id", "personaseccion_id", "seccion_id", "personamanzana_id")

	}

	if !db.HasTable(&CasillaIncidencia{}) {
		db.AutoMigrate(&CasillaIncidencia{}).
			AddForeignKey("incidencia_id", "incidencias(id)", "RESTRICT", "CASCADE").
			AddForeignKey("casilla_id,seccion_id", "casillas(id,seccion_id)",
			"RESTRICT", "CASCADE")
	}

	if !db.HasTable(&Promotores{}) {
		db.AutoMigrate(&Promotores{}).
			AddForeignKey("personapromotor_id", "personas(id)", "RESTRICT", "CASCADE")

		db.Model(&Promotores{}).AddForeignKey("grupo_id,seccion_id,personaseccion_id,personamanzana_id",
			"coordinador_manzanas(grupo_id,seccion_id,personaseccion_id,personamanzana_id)", "RESTRICT", "CASCADE").
			AddUniqueIndex("index_promotor", "grupo_id", "seccion_id",
			"personaseccion_id", "personamanzana_id", "personapromotor_id")
	}

	if !db.HasTable(&Votantes{}) {
		db.AutoMigrate(&Votantes{}).
			AddForeignKey("personavotante_id", "personas(id)", "RESTRICT", "CASCADE")

		db.Model(&Votantes{}).AddForeignKey("grupo_id,seccion_id,personaseccion_id,personamanzana_id,personapromotor_id",
			"promotores(grupo_id,seccion_id,personaseccion_id,personamanzana_id,personapromotor_id)", "RESTRICT", "CASCADE")

	}

}
