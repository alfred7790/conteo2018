package Entidades

import (
	"github.com/jinzhu/gorm"
	"../../Modulos/Cifrado"
	"errors"
)

func ActualizarUsuario(usuario Usuarios, tx *gorm.DB) (*Usuarios, error) {
	if err := tx.Save(&usuario).Error;
		err != nil {
		return nil, err
	}

	return &usuario, nil
}

func LoguearUsuario(nombre string, pass string) (*Usuarios, error) {
	conn := GetDB()
	var user Usuarios
	pass = Cifrado.Cryp(pass)
	if err := conn.Where(Usuarios{Usuario: nombre, Password: pass}).First(&user).Error;
		err != nil {
		return nil, err
	}

	return &user, nil
}

func ObtenerUsuarios() (*[]Usuarios, error) {
	conn := GetDB()

	var usuarios []Usuarios

	if err := conn.Preload("Rol").Find(&usuarios).Error;
		err != nil {
		return nil, err
	}

	return &usuarios, nil
}

func CrearRol(roles Roles) (*Roles, error) {
	conn := GetDB()
	if err := conn.Create(&roles).Error;
		err != nil {
		return nil, errors.New("Error al crear rol")
	}
	return &roles, nil
}
