package Entidades

import (
	"github.com/jinzhu/gorm"
	"fmt"
	"../../Modulos/General"
	"errors"
	"encoding/json"
	"time"
)

func CrearFuncionario(funcionario Funcionarios, tx *gorm.DB) (*Funcionarios, error) {
	var f2 Funcionarios

	if err := tx.Where(&Funcionarios{SeccionId: funcionario.SeccionId, CasillaId: funcionario.CasillaId, Rol: funcionario.Rol}).
		First(&f2).Error;
		gorm.IsRecordNotFoundError(err) {

		if err := tx.Create(&funcionario).Error;
			err != nil {
			return nil, err
		}

		return &funcionario, nil

	} else if err != nil {
		return nil, err
	}

	return nil, errors.New("N")

}

func EliminarFuncionario(funcionario Funcionarios, tx *gorm.DB) (error) {
	if err := tx.Where("persona_id = ? AND seccion_id = ?",
		funcionario.PersonaId, funcionario.SeccionId).Delete(&Funcionarios{}).Error;
		err != nil {
		fmt.Println("Error: ", err)
		return err
	}

	return nil
}

func FindAllFuncionarios(limit, pagina int, consultas []Consulta) ([]*Funcionarios, error) {
	db := GetDB()
	db = db.Set("gorm:auto_preload", true)
	funcionarios := make([]*Funcionarios, 5)

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&funcionarios)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&funcionarios).Error; gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return funcionarios, nil

}

func ReemplazarFuncionario(funcionario Funcionarios, observacion string, tx *gorm.DB) (*Funcionarios, *Funcionarios, error) {
	var f2 Funcionarios

	if err := tx.Where(&Funcionarios{SeccionId: funcionario.SeccionId, CasillaId: funcionario.CasillaId, Rol: funcionario.Rol}).
		First(&f2).Error;
		gorm.IsRecordNotFoundError(err) {
		return nil, nil, err
	} else if err != nil {
		return nil, nil, err
	}

	var f3 FuncionariosHistorial
	b, err := json.Marshal(f2)
	if err != nil {
		return nil, nil, err
	}
	err = json.Unmarshal(b, &f3)
	if err != nil {
		return nil, nil, err
	}
	f3.Fecha = time.Now()
	f3.Observacion = observacion
	if err := tx.Where("persona_id = ? AND seccion_id = ?",
		f2.PersonaId, f2.SeccionId).Delete(&Funcionarios{}).Error;
		err != nil {
		return nil, nil, err
	}

	if err := tx.Create(&funcionario).Error;
		err != nil {
		return nil, nil, err
	}

	if err := tx.Create(&f3).Error;
		err != nil {
		return nil, nil, err
	}
	return &funcionario, &f2, nil

}
