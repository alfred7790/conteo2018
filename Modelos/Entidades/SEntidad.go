package Entidades

import (
	"time"
)

type (
	Personas struct {
		Id                   string               `json:"Id"`
		Nombre               string               `json:"Nombre" gorm:"not null"`
		App                  string               `json:"App" gorm:"not null"`
		Apm                  string               `json:"Apm"`
		Correo               string               `json:"Correo"`
		Facebook             string               `json:"Facebook"`
		Twitter              string               `json:"Twitter"`
		RedSocialExtra       string               `json:"RedSocial"`
		Curp                 string               `json:"Curp"`
		FolioElec            string               `json:"FolioElec"`
		ClaveElec            string               `json:"ClaveElec" gorm:"unique"`
		Rfc                  string               `json:"Rfc"`
		Direccion            Direcciones          `json:"Direccion" gorm:"foreignkey:PersonaId"`
		PersonaCoordinador   PersonaCoordinador   `json:"PersonaCoordinador" gorm:"foreignkey:PersonaId;association_autoupdate:false;association_autocreate:false" `
		PersonaRepresentante PersonaRepresentante `json:"PersonaRepresentante" gorm:"foreignkey:PersonaId;association_autoupdate:false;association_autocreate:false" `
		Telefono             []Telefonos          `json:"Telefonos"  gorm:"foreignkey:PersonaId;"`
		//CoordinadorZona      *[]CoordinadorZonas     `json:"CoordinadorZona" gorm:"foreignkey:PersonaId;association_autoupdate:false;association_autocreate:false"`
		//CoordinadorSeccion   *[]CoordinadorSecciones `json:"CoordinadorSeccion" gorm:"foreignkey:PersonaId;association_autoupdate:false;association_autocreate:false"`
		/*		CoordinadorManzana   *[]CoordinadorManzanas  `json:"CoordinadorManzana" gorm:"foreignkey:PersonaId;association_autoupdate:false;association_autocreate:false"`
				Promotor             *[]Promotores           `json:"Promotor" gorm:"foreignkey:PersonaId;association_autoupdate:false;association_autocreate:false"`
			*/Representante *[]Representantes `json:"Representante;" gorm:"association_autoupdate:false;association_autocreate:false"`
		SeccionId           string            `json:"SeccionId"`
		Agregado            bool              `json:"Agregado"`
		VotoYa              bool              `json:"VotoYa"`
	}

	Direcciones struct {
		Id             string  `gorm:"primary_key"`
		Calle          string  `json:"Calle" gorm:"not null"`
		NumeroExterior string  `json:"NumeroExterior" `
		NumeroInterior *string `json:"NumeroInterior" `
		Colonia        string  `json:"Colonia"  `
		Municipio      string  `json:"Municipio" `
		CodigoPostal   string  `json:"CodigoPostal" `
		Referencias    *string `json:"Referencias"`
		LugarUbicacion *string `json:"LugarUbicacion"`
		TipoDomicilio  *string `json:"TipoDomicilio"`
		PersonaId      *string `json:"PersonaId"`
	}

	Telefonos struct {
		Id        string `json:"Id" gorm:"primary_key"`
		PersonaId string `json:"PersonaId"`
		Numero    string `json:"Numero"  `
		Tipo      string `json:"Tipo"`
	}

	Secciones struct {
		Id            string     `json:"Id"`
		Casillas      []Casillas `json:"Casillas" gorm:"foreignkey:SeccionId"`
		TipoSeccion   string     `json:"TipoSeccion" gorm:"not null"`
		Observaciones *string    `json:"Observaciones"`
		Municipio     string     `json:"Municipio"`
		Distrito      string     `json:"Distrito"`
		Vigente       bool       `json:"Vigente" gorm:"default:true"`
	}

	Casillas struct {
		Id                string              `json:"Id" gorm:"primary_key"`
		SeccionId         string              `json:"SeccionId" gorm:"primary_key"`
		TipoCasilla       string              `json:"TipoCasilla"`
		Direccion         Direcciones         `json:"Direccion"`
		DireccionId       string              `json:"DireccionId"`
		VotosTotales      *int                `json:"VotosTotales"`
		VotosNulos        *int                `json:"VotosNulos"`
		VotosBuenos       *int                `json:"VotosBuenos"`
		VotosEstimados    *int                `json:"VotosEstimados"`
		PadronElectoral   int                 `json:"PadronElectoral"`
		ListaNominal      int                 `json:"ListaNominal"`
		HoraApertura      *time.Time          `json:"HoraApertura"`
		HoraCierre        *time.Time          `json:"HoraCierre"`
		Observaciones     *string             `json:"Observaciones"`
		Votos             []Votos             `json:"Votos"`
		CasillaIncidencia []CasillaIncidencia `json:"CasillaIncidencia" gorm:"foreign:CasillaId"`
		Vigente           bool                `json:"Vigente" gorm:"default:true"`
	}

	CasillaIncidencia struct {
		CasillaId    string      `json:"CasillaId" gorm:"primary_key"`
		SeccionId    string      `json:"SeccionId" gorm:"primary_key"`
		Incidencia   Incidencias `json:"Incidencia"`
		IncidenciaId int         `json:"IncidenciaId" gorm:"primary_key"`
	}

	CasillaDireccion struct {
		CasillaId   string    `json:"CasillaId" `
		SeccionId   string    `json:"SeccionId" `
		DireccionId string    `json:"DireccionId"`
		Observacion string    `json:"Observacion"`
		Hora        time.Time `json:"Hora"`
	}


	Representantes struct {
		Persona   Personas `gorm:"association_autoupdate:false;association_autocreate:false"`
		PersonaId string   `json:"PersonaId" gorm:"not null;primary_key;unique"`
		Partido   Partidos `json:"Partido" gorm:"association_autoupdate:false;association_autocreate:false"`
		PartidoId string   `json:"PartidoId" gorm:"not null;primary_key"`
		CasillaId string   `json:"CasillaId" gorm:"not null;primary_key"`
		SeccionId string   `json:"SeccionId" gorm:"not null;primary_key"`
	}

	Partidos struct {
		Id            string   `json:"Id"`
		Nombre        string   `json:"Nombre" gorm:"not null,unique"`
		Abreviaciones string   `json:"Abreviaciones" gorm:"unique"`
		Persona       Personas `json:"Persona"`
		PersonaId     string   `json:"PersonaId"`
		Orden         int      `json:"Orden" gorm:"unique;AUTO_INCREMENT"`
		Coalicion     bool     `json:"Coalicion"`
	}

	Partidos2 struct {
		Id            string `json:"Id"`
		Nombre        string `json:"Nombre" gorm:"not null"`
		Abreviaciones string `json:"Abreviaciones" gorm:"unique"`
		PersonaId     string `json:"PersonaId"`
		Orden         int    `json:"Orden" gorm:"unique"`
	}

	Votos struct {
		Partido     Partidos `json:"Partido"`
		PartidoId   string   `json:"PartidoId" gorm:"primary_key"`
		CasillaId   string   `json:"CasillaId" gorm:"primary_key"`
		SeccionId   string   `json:"SeccionId" gorm:"primary_key"`
		NumeroVotos int      `json:"NumeroVotos" gorm:"not null"`
	}

	Votos2 struct {
		Partido     Partidos `json:"Partido"`
		PartidoId   string   `json:"PartidoId" gorm:"primary_key"`
		CasillaId   string   `json:"CasillaId" gorm:"primary_key"`
		SeccionId   string   `json:"SeccionId" gorm:"primary_key"`
		NumeroVotos int      `json:"NumeroVotos" gorm:"not null"`
	}

	Grupos struct {
		Id           string      `json:"Id"`
		Clave        string      `json:"Clave" gorm:"unique"`
		Area         string      `json:"Area" `
		Zona         string      `json:"Zona"`
		Partido      Partidos    `json:"Partido"`
		PartidoId    string      `json:"PartidoId" gorm:"not null"`
		Persona      Personas    `json:"Persona"`
		PersonaId    string      `json:"PersonaId"`
		Estructura   Estructuras `json:"Estructura"`
		EstructuraId string      `json:"EstructuraId"`
	}

	CoordinadorZonas struct {
		Personazona   Personas `json:"Personazona"`
		PersonazonaId string   `json:"PersonazonaId" gorm:"primary_key"`
		GrupoId       string   `json:"GrupoId" gorm:"primary_key"`
	}

	CoordinadorSecciones struct {
		GrupoId          string   `json:"GrupoId" gorm:"primary_key"`
		SeccionId        string   `json:"SeccionId" gorm:"primary_key"`
		Personaseccion   Personas `json:"Personaseccion"`
		PersonaseccionId string   `json:"PersonaseccionId" gorm:"primary_key;unique"`
	}

	CoordinadorManzanas struct {
		GrupoId          string `json:"GrupoId" gorm:"primary_key"`
		SeccionId        string `json:"SeccionId" gorm:"primary_key"`
		PersonaseccionId string `json:"PersonaseccionId" gorm:"primary_key"`
		PersonamanzanaId string `json:"PersonamanzanaId" gorm:"primary_key;unique"`
	}

	Promotores struct {
		GrupoId           string `json:"GrupoId" gorm:"primary_key"`
		SeccionId         string `json:"SeccionId" gorm:"primary_key"`
		PersonaseccionId  string `json:"PersonaseccionId" gorm:"primary_key"`
		PersonamanzanaId  string `json:"PersonamanzanaId" gorm:"primary_key"`
		PersonapromotorId string `json:"PersonapromotorId" gorm:"primary_key;unique"`
	}

	Votantes struct {
		GrupoId           string `json:"GrupoId" gorm:"primary_key"`
		SeccionId         string `json:"SeccionId" gorm:"primary_key"`
		PersonaseccionId  string `json:"PersonaseccionId" gorm:"primary_key"`
		PersonamanzanaId  string `json:"PersonamanzanaId" gorm:"primary_key"`
		PersonapromotorId string `json:"PersonapromotorId" gorm:"primary_key"`
		PersonavotanteId  string `json:"PersonavotanteId" gorm:"primary_key"`
		VotoFinal         bool   `json:"VotoFinal" `
	}

	Usuarios struct {
		Id       int    `json:"Id"`
		Usuario  string `json:"Usuario" gorm:"not null;unique"`
		Password string `json:"Password" gorm:"not null"`
		Rol      Roles  `json:"Roles"`
		RolId    int    `json:"RolId"  gorm:"not null"`
		Nombre   string `json:"Nombre" gorm:"not null"`
	}

	Movimientos struct {
		ActividadId int       `json:"ActividadId"`
		Fecha       time.Time `json:"Fecha" gorm:"not null"`
		UsuarioId   int       `json:"UsuarioId" gorm:"not null"`
		Extra       string    `json:"Extra"`
		Extra2      string    `json:"Extra2"`
		Observacion string    `json:"Observacion"`
	}

	Actividades struct {
		Id          int    `json:"Id" `
		Nombre      string `json:"Nombre" gorm:"not null"`
		Descripcion string `json:"Descripcion"`
	}

	Funcionarios struct {
		Persona   Personas `gorm:"association_autoupdate:false;association_autocreate:false"`
		PersonaId string   `json:"PersonaId"  gorm:"unique"`
		CasillaId string   `json:"CasillaId" gorm:"primary_key"`
		SeccionId string   `json:"SeccionId" gorm:"primary_key"`
		Rol       string   `json:"Rol" gorm:"primary_key"`
	}

	FuncionariosHistorial struct {
		Persona     Personas  `gorm:"association_autoupdate:false;association_autocreate:false"`
		PersonaId   string    `json:"PersonaId"`
		CasillaId   string    `json:"CasillaId"`
		SeccionId   string    `json:"SeccionId"`
		Rol         string    `json:"Rol"`
		Fecha       time.Time `json:"Fecha"`
		Observacion string    `json:"Observacion"`
	}

	Roles struct {
		Id   int    `json:"Id"`
		Tipo string `json:"Tipo"`
	}

	TipoCoordinador struct {
		Id          int    `json:"Id"`
		Descripcion string `json:"Descripcion"`
	}

	PersonaCoordinador struct {
		TipoCoordinadorId int     `json:"TipoCoordinadorId"`
		PersonaId         string  `json:"PersonaId" gorm:"unique"`
		Meta              int     `json:"Meta"`
		PartidoId         *string `json:"PartidoId"`
	}

	PersonaRepresentante struct {
		TipoRepresentanteId int    `json:"TipoRepresentanteId"`
		PersonaId           string `json:"PersonaId" gorm:"unique"`
	}

	Incidencias struct {
		Id          int    `json:"Id"`
		Descripcion string `json:"Descripcion"`
	}

	CasillaHora struct {
		Hora      time.Time `json:"Hora"`
		Apertura  bool      `json:"Apertura"`
		CasillaId string    `json:"CasillaId"`
		SeccionId string    `json:"SeccionId"`
	}

	Estructuras struct {
		Id          string `json:"Id"`
		Nombre      string `json:"Nombre" gorm:"unique"`
		Observacion string `json:"Observacion"`
	}


)
