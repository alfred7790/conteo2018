package Entidades

import (
	"github.com/jinzhu/gorm"
	"fmt"
	"../../Modulos/General"
	"errors"
)

func FindListaCoordinadoresSeccion(limit, pagina int, consultas []Consulta) (*[]ListaCoordinadoresSeccion, error) {
	db := GetDB()
	var personas []ListaCoordinadoresSeccion

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&personas)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&personas).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return &personas, nil

}

func FindListaCoordinadoresManzana(limit, pagina int, consultas []Consulta) (*[]ListaCoordinadoresManzana, error) {
	db := GetDB()
	var personas []ListaCoordinadoresManzana

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&personas)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&personas).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return &personas, nil

}

func FindListaPromotores(limit, pagina int, consultas []Consulta) (*[]ListaPromotores, error) {
	db := GetDB()
	var personas []ListaPromotores

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&personas)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&personas).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return &personas, nil

}
