package Entidades

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/jinzhu/gorm"
	"log"
	"../../Modulos/Variables"
	"strings"
	"github.com/gin-gonic/gin"
	"strconv"
	"encoding/json"
	"github.com/kataras/iris/core/errors"
	"fmt"
)

type Consulta struct {
	Valor    interface{}
	Valor2   interface{}
	Campo    string
	Campo2   string
	Operador string
	And      bool
	Orden    int
}

var config = MoVar.CargaSeccionCFG(MoVar.SecPsql)

func init() {

	var err error
	db, err = gorm.Open("postgres", "host="+config.Servidor+" port="+config.Puerto+" user="+config.Usuario+" dbname="+config.NombreBase+" password="+config.Pass+" sslmode=disable")

	if err != nil {
		fmt.Println("ERR: ", err)
		log.Fatal("Existió un error en la conexión a la base de datos.", err)
	}
	fmt.Println("Se  conecto a la bd correctamente")
	db.SingularTable(true)
	db.LogMode(false)

	CrearTablas()

}

var (
	db *gorm.DB
)

func GetDB() *gorm.DB {
	return db
}

func BuildQuery(db *gorm.DB, filtros []Consulta) *gorm.DB {
	//db = db.Not("tipo = ?", "CANDIDATO DE PARTIDO")
	for _, filtro := range filtros {
		if filtro.Valor == nil || filtro.Valor == "" {
			continue
		}
		if strings.Contains(filtro.Operador, "like") {
			filtro.Valor = "%" + filtro.Valor.(string) + "%"
		}
		//fmt.Println("Campo 2", filtro.Campo2)
		//fmt.Println("Valor 2: ", filtro.Valor2)
		if filtro.Campo2 != "" {
			//	fmt.Println("Campo 2 tiene algo")
			db = db.Where(filtro.Campo+" "+filtro.Operador+" ? and "+filtro.Campo2+" = ?", filtro.Valor, filtro.Valor2)
		} else {
			//	fmt.Println("Se hizo el segundo caso")
			if filtro.And {
				db = db.Where(filtro.Campo+" "+filtro.Operador+" ?", filtro.Valor)
			} else {
				db = db.Or(filtro.Campo+" "+filtro.Operador+" ?", filtro.Valor)
			}
		}
	}

	for _, filtro := range filtros {
		switch filtro.Orden {
		case -1:
			db = db.Order(filtro.Campo + " desc")
		case 1:
			db = db.Order(filtro.Campo + " asc")
		}
	}

	return db
}

func GetFiltros(ctx *gin.Context) ([]Consulta, error) {

	cadena := ctx.DefaultQuery("filtros", "")
	if cadena == "" {
		return nil, errors.New("El filtro esta vacio")
	}
	var filtros []Consulta
	err := json.Unmarshal([]byte(cadena), &filtros)
	if err != nil {
		return nil, err
	}
	return filtros, nil
}

func GetLimitPage(ctx *gin.Context) (limit, page int) {
	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "50"))
	if err != nil || limit <= 0 {
		limit = 50
	}
	if limit > 100 {
		limit = 100
	}
	page, err = strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil || page == 0 {
		page = 1
	}
	return
}
