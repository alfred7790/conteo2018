package Entidades

import "time"

type (
	CasillaDireccionHistorial struct {
		CasillaId    string `json:"CasillaId"`
		SeccionId    string `json:"SeccionId"`
		Calle        string `json:"Calle"`
		Colonia      string `json:"Colonia"`
		CodigoPostal string `json:"CodigoPostal"`
	}

	CasillaCambioDireccion struct {
		CasillaId   string      `json:"CasillaId" `
		SeccionId   string      `json:"SeccionId" `
		Direccion   Direcciones `json:"Direccion"`
		Observacion string      `json:"Observacion"`
	}

	FuncionarioCasillaHistorial struct {
		CasillaId              string `json:"CasillaId"`
		SeccionId              string `json:"SeccionId"`
		Rol                    string `json:"Rol"`
		NombreFuncNuevo        string `json:"NombreFuncNuevo"`
		AppFuncNuevo           string `json:"AppFuncNuevo"`
		ApmFuncNuevo           string `json:"ApmFuncNuevo"`
		ClaveElectFuncNuevo    string
		AppFuncAnterior        string
		ApmFuncAnterior        string
		ClaveElectFuncAnterior string
	}

	ListaCoordinadores struct {
		Persona     Personas `json:"Persona"`
		PersonaId   string   `json:"PersonaId"`
		Nombre      string   `json:"Nombre"`
		App         string   `json:"App"`
		Apm         string   `json:"Apm"`
		ClaveElec   string   `json:"ClaveElec"`
		SeccionId   string   `json:"SeccionId"`
		Tipo        string   `json:"Tipo"`
		PartidoId   string   `json:"PartidoId"`
		Partido     string   `json:"Partido"`
		Abreviacion string   `json:"Abreviacion"`
	}

	ListaRepresentantes struct {
		Id     string `json:"Id"`
		Nombre string `json:"Nombre"`
		App    string `json:"App"`
		Apm    string `json:"Apm"`
		Tipo   string `json:"Tipo"`
	}

	ListaCoordinadoresSeccion struct {
		Id                 string `json:"Id"`
		Nombre             string `json:"Nombre"`
		App                string `json:"App"`
		Apm                string `json:"Apm"`
		ClaveElec          string `json:"ClaveElec"`
		SeccionId          string `json:"SeccionId"`
		GrupoId            string `json:"GrupoId"`
		GrupoClave         string `json:"GrupoClave"`
		PartidoId          string `json:"PartidoId"`
		PartidoNombre      string `json:"PartidoNombre"`
		PartidoAbreviacion string `json:"PartidoAbreviacion"`
	}

	ListaCoordinadoresManzana struct {
		Id                   string `json:"Id"`
		Nombre               string `json:"Nombre"`
		App                  string `json:"App"`
		Apm                  string `json:"Apm"`
		ClaveElec            string `json:"ClaveElec"`
		SeccionId            string `json:"SeccionId"`
		GrupoId              string `json:"GrupoId"`
		PartidoId            string `json:"PartidoId"`
		PartidoNombre        string `json:"PartidoNombre"`
		PartidoAbreviacion   string `json:"PartidoAbreviacion"`
		PersonaseccionId     string `json:"PersonaseccionId"`
		PersonaseccionNombre string `json:"PersonaseccionNombre"`
		PersonaseccionApp    string `json:"PersonaseccionApp"`
		PersonaseccionApm    string `json:"PersonaseccionApm"`
	}

	ListaPromotores struct {
		Id                   string `json:"Id"`
		Nombre               string `json:"Nombre"`
		App                  string `json:"App"`
		Apm                  string `json:"Apm"`
		ClaveElec            string `json:"ClaveElec"`
		PartidoId            string `json:"PartidoId"`
		PartidoNombre        string `json:"PartidoNombre"`
		PartidoAbreviacion   string `json:"PartidoAbreviacion"`
		GrupoId              string `json:"GrupoId"`
		SeccionId            string `json:"SeccionId"`
		PersonaseccionId     string `json:"PersonaseccionId"`
		PersonamanzanaId     string `json:"PersonamanzanaId"`
		PersonamanzanaNombre string `json:"PersonamanzanaNombre"`
		PersonamanzanaApp    string `json:"PersonamananaApp"`
		PersonamanzanaApm    string `json:"PersonamanzanaApm"`
	}

	ListaPromovidos struct {
		Id                    string `json:"Id"`
		Nombre                string `json:"Nombre"`
		App                   string `json:"App"`
		Apm                   string `json:"Apm"`
		ClaveElec             string `json:"ClaveElec"`
		GrupoId               string `json:"GrupoId"`
		SeccionId             string `json:"SeccionId"`
		PersonaseccionId      string `json:"PersonaseccionId"`
		PersonamanzanaId      string `json:"PersonamanzanaId"`
		PartidoId             string `json:"PartidoId"`
		PartidoNombre         string `json:"PartidoNombre"`
		PartidoAbreviacion    string `json:"PartidoAbreviacion"`
		PersonapromotorId     string `json:"PersonapromotorId"`
		PersonapromotorNombre string `json:"PersonapromotorNombre"`
		PersonapromotorApp    string `json:"PersonapromotorApp"`
		PersonapromotorApm    string `json:"PersonapromotorApm"`
	}

	CoordPorDistrito struct {
		Distrito          string `json:"Distrito"`
		Cargo             string `json:"Cargo"`
		TotalPromotor     int    `json:"TotalPromotor"`
		Cargoo            string `json:"Cargoo"`
		TotalCoordmanzana int    `json:"TotalCoordmanzana"`
	}

	CoordPorDistrito2 struct {
		Distrito          string `json:"Distrito"`
		TotalPromotor     int    `json:"TotalPromotor"`
		TotalCoordmanzana int    `json:"TotalCoordmanzana"`
	}

	CoordPorSeccionDistrito struct {
		Distrito  string `json:"Distrito"`
		SeccionId string `json:"SeccionId"`
		Tipo      string `json:"TipoManzana"`
		Total     int    `json:"TotalManzana"`
		Tipoo     string `json:"TipoPromotor"`
		Totaal    int    `json:"TotalPromotor"`
	}

	CoordPorSeccionDistrito2 struct {
		Distrito  string `json:"Distrito"`
		SeccionId string `json:"SeccionId"`
		Total     int    `json:"TotalManzana"`
		Totaal    int    `json:"TotalPromotor"`
	}

	ReporteTotalDistrito struct {
		Distrito string `json:"Distrito"`
		Tipo     string `json:"Tipo"`
		Partido  string `json:"Partido"`
		Total    int    `json:"Total"`
	}

	ReporteTotalDistritoSeccion struct {
		Distrito string `json:"Distrito"`
		Seccion  string `json:"Seccion"`
		Tipo     string `json:"Tipo"`
		Partido  string `json:"Partido"`
		Total    int    `json:"Total"`
	}

	ListaPromovidosRepetidos struct {
		Nombre                string `json:"Nombre"`
		App                   string `json:"App"`
		Apm                   string `json:"Apm"`
		SeccionId             string `json:"SeccionId"`
		PartidoAbreviacion    string `json:"PartidoAbreviacion"`
		PersonapromotorNombre string `json:"PersonapromotorNombre"`
		PersonapromotorApp    string `json:"PersonapromotorApp"`
		PersonapromotorApm    string `json:"PersonapromotorApm"`
	}

	ConcentradoTotal struct {
		Tipo  string `json:"name"`
		Total int    `json:"value"`
	}

	PromovidosPorEstructura struct {
		Nombre string `json:"name"`
		Total  int    `json:"value"`
	}

	PromovidosPorEstructuraSeccion struct {
		Estructura string `json:"name"`
		Seccion    string `json:"value"`
		Total      int    `json:"total"`
	}

	TotalPromovidos struct {
		Tipo  string `json:"name"`
		Total int    `json:"value"`
	}

	PromovidosRepetidosEstructuras struct {
		Nombre                string `json:"Nombre"`
		App                   string `json:"App"`
		Apm                   string `json:"Apm"`
		ClaveElec             string `json:"ClaveElec"`
		EstructuraNombre      string `json:"EstructuraNombre"`
		SeccionId             string
		PersonapromotorNombre string
		PersonapromotorApp    string
		PersonapromotorApm    string
	}

	CasillasConIncidencias struct {
		CasillaId    string `json:"CasillaId"`
		SeccionId    string `json:"SeccionId"`
		Calle        string `json:"Calle"`
		Colonia      string `json:"Colonia"`
		CodigoPostal string `json:"CodigoPostal"`
		Descripcion  string `json:"Descripcion"`
	}

	EstadoCasillas struct {
		Estado string `json:"name"`
		Total  int    `json:"value"`
	}

	EstadoCasillasCerradas struct {
		Estado string `json:"name"`
		Total  int    `json:"value"`
	}

	CoordinadorMppSeccion struct {
		NombreCoordManzana    string
		NombreAppCoordManzana string
		NombreApmCoordManzana string
		NombrePromotor        string
		NombreApp             string
		NombreApm             string
		ClaveElec             string
		NombrePromovido       string
		NombreAppPromovido    string
		NombreApmPromovido    string
		ClaveElecPromovido    string
		EstructuraNombre      string
		SeccionId             string
		PartidoAbreviacion    string
	}

	VotosEstimados struct {
		CasillaId      string `json:"CasillaId"`
		SeccionId      string `json:"SeccionId"`
		VotosEstimados int    `json:"VotosEstimados"`
	}

	VotosPorPartido struct {
		Partido   string `json:"name"`
		Total     int    `json:"value"`
		Coalicion bool   `json:"Coalicion"`
	}

	TotalVotosCandidato struct {
		Nombre string `json:"Nombre"`
		App    string `json:"App"`
		Apm    string `json:"Apm"`
		Total  int    `json:"value"`
	}

	CasillasSinregistrarVotos struct {
		Casilla      string    `json:"Casilla"`
		Seccion      string    `json:"Seccion"`
		ListaNominal int       `json:"ListaNominal"`
		HoraApertura time.Time `json:"HoraApertura"`
		HoraCierre   time.Time `json:"HoraCierre"`
	}

	VotosCasillas struct {
		Casilla string `json:"name"`
		Total   int    `json:"value"`
	}

	VotosPartidoSeccion struct {
		Seccion string `json:"Seccion"`
		Partido string `json:"name"`
		Total   int    `json:"value"`
	}

	VotosPartidoColonia struct {
		Seccion string `json:"Seccion"`
		Colonia string `json:"Colonia"`
		Partido string `json:"name"`
		Total   int    `json:"value"`
	}

	FlujoVotosEstimados struct {
		Seccion string `json:"name"`
		Sum   int    `json:"value"`
	}

	Colonias struct {
		Colonia string `json:"Colonia"`
	}
)
