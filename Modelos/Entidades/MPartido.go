package Entidades

import (
	"github.com/jinzhu/gorm"
	"../../Modulos/General"
	"fmt"
	"errors"
)

func CrearPartido(partido Partidos, tx *gorm.DB) (*Partidos, error) {
	if err := tx.Create(&partido).Error;
		err != nil {
		return nil, err
	}

	return &partido, nil
}

func ObtenerPartidos() (*[]Partidos, error) {
	conn := GetDB()

	var partidos []Partidos
	if err := conn.Preload("Persona").Find(&partidos).Error;
		err != nil {
		return nil, err
	}

	return &partidos,nil
}

func FindAllPartidos(limit, pagina int, consultas []Consulta) ([]*Partidos, error) {
	db := GetDB()
	partidos := make([]*Partidos, 5)

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&partidos)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&partidos).Error; gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return partidos, nil

}


func CrearPartido2(partido Partidos2, tx *gorm.DB) (*Partidos2, error) {
	if err := tx.Create(&partido).Error;
		err != nil {
		return nil, err
	}

	return &partido, nil
}

func ObtenerPartidos2() (*[]Partidos2, error) {
	conn := GetDB()

	var partidos []Partidos2
	if err := conn.Find(&partidos).Error;
		err != nil {
		return nil, err
	}

	return &partidos,nil
}

func FindAllPartidos2(limit, pagina int, consultas []Consulta) ([]*Partidos2, error) {
	db := GetDB()
	partidos := make([]*Partidos2, 5)

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&partidos)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&partidos).Error; gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return partidos, nil

}




