package Entidades

import (
	"github.com/jinzhu/gorm"
	"fmt"
	"../../Modulos/General"
	"errors"
)

func FindAllPersonas(limit, pagina int, consultas []Consulta) ([]*Personas, error) {
	db := GetDB()

	db = db.Preload("Direccion").Preload("Telefono").Preload("PersonaCoordinador").Preload("PersonaRepresentante")

	personas := make([]*Personas, 5)

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&personas)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&personas).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return personas, nil

}

func CrearPersona(persona Personas, tx *gorm.DB) (*Personas, error) {
	if err := tx.Create(&persona).Error;
		err != nil {
		return nil, err
	}

	return &persona, nil
}

func ActualizarPersona(persona Personas, tx *gorm.DB) (*Personas, error) {
	if err := tx.Save(&persona).Error;
		err != nil {
		return nil, err
	}
	return &persona, nil

}

func CrearUsuario(usuario Usuarios, tx *gorm.DB) (*Usuarios, error) {

	if err := tx.Create(&usuario).Error;
		err != nil {
		fmt.Println(err)
		return nil, err
	}

	return &usuario, nil
}

func AgregarCoordinador(coordinador PersonaCoordinador, tx *gorm.DB) (*Personas, error) {
	if err := tx.Create(&coordinador).Error;
		err != nil {
		fmt.Println(err)
		return nil, err
	}

	var persona Personas
	tx.Set("gorm:auto_preload", true).Where(&Personas{Id: coordinador.PersonaId}).First(&persona)

	return &persona, nil
}

func EliminarTelefono(telefono Telefonos, tx *gorm.DB) (error) {
	if err := tx.Delete(telefono).Error;
		err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func EliminarCoordinador(coordinador PersonaCoordinador, tx *gorm.DB) (*Personas, error) {
	if err := tx.Where("persona_id = ?", coordinador.PersonaId).Delete(&PersonaCoordinador{}).Error;
		err != nil {
		fmt.Println(err)
		return nil, err
	}

	var persona Personas
	tx.Set("gorm:auto_preload", true).Where(&Personas{Id: coordinador.PersonaId}).First(&persona)

	return &persona, nil
}

func ActualizarCoordinador(persona PersonaCoordinador, tx *gorm.DB) (*PersonaCoordinador, error) {
	var persona2 PersonaCoordinador
	if err := tx.Where("persona_id = ?", persona.PersonaId).First(&persona2).Error;
		err != nil {
		return nil, err
	}

	if persona2.TipoCoordinadorId != persona.TipoCoordinadorId {
		return nil, errors.New("Primero asigne el rol a ninguno")
	}

	if err := tx.Table("persona_coordinador").Where("persona_id = ?",
		persona.PersonaId).UpdateColumns(PersonaCoordinador{Meta: persona.Meta, PartidoId: persona.PartidoId}).Error;
		err != nil {
		return nil, err
	}
	return &persona, nil

}

func FindListaCoordinadores(limit, pagina int, consultas []Consulta) (*[]ListaCoordinadores, error) {
	db := GetDB()
	db = db.Preload("Persona")
	var personas []ListaCoordinadores

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&personas)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&personas).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return &personas, nil

}

func ValidarClaveElector(clave string) error {
	db := GetDB()

	var persona Personas

	if err := db.Where("clave_elec = ?", clave).First(&persona).Error;
		gorm.IsRecordNotFoundError(err) {
		return nil
	} else {
		return errors.New("Ya se encuentra registrada esa clave de elector")
	}
}

func ValidarCurp(curp string) error {
	db := GetDB()

	var persona Personas

	if err := db.Where("curp = ?", curp).First(&persona).Error;
		gorm.IsRecordNotFoundError(err) {
		return nil
	} else {
		return errors.New("Ya se encuentra registrada esa curp")
	}
}

func AgregarRepresentante(representante PersonaRepresentante, tx *gorm.DB) (*Personas, error) {
	if err := tx.Create(&representante).Error;
		err != nil {
		return nil, err
	}

	var persona Personas
	tx.Set("gorm:auto_preload", true).Where(&Personas{Id: representante.PersonaId}).First(&persona)

	return &persona, nil
}

func EliminarRepresentante(representante PersonaRepresentante, tx *gorm.DB) (*Personas, error) {
	if err := tx.Where("persona_id = ?", representante.PersonaId).Delete(&PersonaRepresentante{}).Error;
		err != nil {
		return nil, err
	}

	var persona Personas
	tx.Set("gorm:auto_preload", true).Where(&Personas{Id: representante.PersonaId}).First(&persona)

	return &persona, nil
}

func FindListaRepresentantes(limit, pagina int, consultas []Consulta) (*[]ListaRepresentantes, error) {
	db := GetDB()

	var personas []ListaRepresentantes

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&personas)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&personas).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return &personas, nil

}

func RegistrarVoto(personaId string, tx *gorm.DB)(error){
	var persona Personas

	if err := tx.Where("id = ?",personaId).Find(&persona).Error;
		err != nil {
		return err
	}

	var voto bool
	if persona.VotoYa{
		voto = false
	}else{
		voto  = true
	}

	if err := tx.Model(&persona).Update("voto_ya",voto).Error;
		err != nil {
		return err
	}

	return nil
}