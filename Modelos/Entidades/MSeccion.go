package Entidades

import (
	"github.com/jinzhu/gorm"
	"fmt"
	"../../Modulos/General"
	"errors"
)

func CrearSeccion(seccion Secciones, tx *gorm.DB) (*Secciones, error) {

	if err := tx.Create(&seccion).Error;
		err != nil {
		return nil, err
	}

	return &seccion, nil
}

func BuscarSeccion(id int) (*Secciones, error) {
	conn := GetDB()
	var seccion Secciones
	if err := conn.First(&seccion).Error;
		err != nil {
		fmt.Println("Problema al buscar seccion ", err)
		return nil, err
	}

	return &seccion, nil
}

func BuscarSeccion2(id string) (*Secciones, error) {
	conn := GetDB()
	var seccion Secciones
	if err := conn.Where("id = ?",id).First(&seccion).Error;
		err != nil {
		return nil, err
	}

	return &seccion, nil
}

func GetAllSeccciones() (*[]Secciones, error) {
	conn := GetDB()
	var seccion []Secciones
	if err := conn.Find(&seccion).Error;
		err != nil {
		fmt.Println("Problema al buscar secciones ", err)
		return nil, err
	}

	return &seccion, nil

}

func ActualizarSeccion(seccion Secciones, tx *gorm.DB) (*Secciones, error) {

	if err := tx.Update(&seccion).Error;
		err != nil {
		fmt.Println("Problema al actualizar seccion ", err)
		return nil, err
	}

	return &seccion, nil

}

func CancelarSeccion(seccion Secciones, tx *gorm.DB) (*Secciones, error) {
	if err := tx.Model(&seccion).Update("vigente", false).Error;
		err != nil {
		return nil, err
	}

	return &seccion, nil
}

func FindAllSecciones(limit, pagina int, consultas []Consulta) ([]*Secciones, error) {
	db := GetDB()
	db = db.Set("gorm:auto_preload", true)
	secciones := make([]*Secciones, 5)

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&secciones)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&secciones).Error; gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return secciones, nil

}

func IniciarTransaccion() (*gorm.DB) {
	return GetDB().Begin()
}
