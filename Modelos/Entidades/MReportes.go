package Entidades

import (
	"github.com/jinzhu/gorm"
	"../../Modulos/General"
	"fmt"
	"errors"
)

func ObtenerCoordxDistrito() (*[]CoordPorDistrito, error) {
	db := GetDB()

	var coor []CoordPorDistrito

	if err := db.Find(&coor).Error;
		err != nil {
		return nil, err
	}
	return &coor, nil
}

func ObtenerCoordxSeccionDistrito() (*[]CoordPorSeccionDistrito, error) {
	db := GetDB()

	var coor []CoordPorSeccionDistrito

	if err := db.Find(&coor).Error;
		err != nil {
		return nil, err
	}
	return &coor, nil
}

func ObtenerReporteTotalDistrito() (*[]ReporteTotalDistrito, error) {
	db := GetDB()

	var coor []ReporteTotalDistrito

	if err := db.Find(&coor).Error;
		err != nil {
		return nil, err
	}
	return &coor, nil
}

func ObtenerReporteTotalDistritoSeccion() (*[]ReporteTotalDistritoSeccion, error) {
	db := GetDB()

	var coor []ReporteTotalDistritoSeccion

	if err := db.Find(&coor).Error;
		err != nil {
		return nil, err
	}
	return &coor, nil
}

func ObtenerReportePromovidosRepetidos() (*[]ListaPromovidosRepetidos, error) {
	db := GetDB()

	var coor []ListaPromovidosRepetidos

	if err := db.Find(&coor).Error;
		err != nil {
		return nil, err
	}
	return &coor, nil
}

func ObtenerConcentradoTotal() (*[]ConcentradoTotal, error) {
	db := GetDB()

	var total []ConcentradoTotal

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerVotosTotalesPartido() (*[]VotosPorPartido, error) {
	db := GetDB()

	var total []VotosPorPartido

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerVotosTotalesCandidato() (*[]TotalVotosCandidato, error) {
	db := GetDB()

	var total []TotalVotosCandidato

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerPromovidosPorEstructura() (*[]PromovidosPorEstructura, error) {
	db := GetDB()

	var total []PromovidosPorEstructura

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerPromovidosPorEstructuraSeccion() (*[]PromovidosPorEstructuraSeccion, error) {
	db := GetDB()

	var total []PromovidosPorEstructuraSeccion

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerCasillasIncidencias() (*[]CasillasConIncidencias, error) {
	db := GetDB()

	var total []CasillasConIncidencias

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerCoordMpp() (*[]CoordinadorMppSeccion, error) {
	db := GetDB()

	var total []CoordinadorMppSeccion

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerCasillaDireccionHistorial() (*[]CasillaDireccionHistorial, error) {
	db := GetDB()

	var total []CasillaDireccionHistorial

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerFuncionarioCasillaHistorial() (*[]FuncionarioCasillaHistorial, error) {
	db := GetDB()

	var total []FuncionarioCasillaHistorial

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerTotalPromovidos() (*[]TotalPromovidos, error) {
	db := GetDB()

	var total []TotalPromovidos

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerPromovidosRepetidosEstructura() (*[]PromovidosRepetidosEstructuras, error) {
	db := GetDB()

	var total []PromovidosRepetidosEstructuras

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerEstadoCasillas() (*[]EstadoCasillas, error) {
	db := GetDB()

	var total []EstadoCasillas

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerEstadoCasillasCerradas() (*[]EstadoCasillasCerradas, error) {
	db := GetDB()

	var total []EstadoCasillasCerradas

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerVotosCasillas() (*[]VotosCasillas, error) {
	db := GetDB()

	var total []VotosCasillas

	if err := db.Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func FindVotosPartidoSeccion(limit, pagina int, consultas []Consulta) (*[]VotosPartidoSeccion, error) {
	db := GetDB()
	var partidos []VotosPartidoSeccion

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&partidos)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&partidos).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return &partidos, nil

}

func ObtenerVotosPartidoSeccion(id string) (*[]VotosPartidoSeccion, error) {
	db := GetDB()

	var total []VotosPartidoSeccion

	if err := db.Where("seccion = ?",id).Find(&total).Error;
		err != nil {
		return nil, err
	}
	return &total, nil
}

func ObtenerCasillasSinRegistrarVotos() (*[]CasillasSinregistrarVotos, error) {
	db := GetDB()

	var casillas []CasillasSinregistrarVotos

	if err := db.Find(&casillas).Error;
		err != nil {
		return nil, err
	}
	return &casillas, nil
}

func ObtenerFlujoVotosEstimados() (*[]FlujoVotosEstimados, error) {
	db := GetDB()

	var votos []FlujoVotosEstimados

	if err := db.Find(&votos).Error;
		err != nil {
		return nil, err
	}

	for _,sec := range votos{
		sec.Seccion = "SEC-"+sec.Seccion
	}
	return &votos, nil
}

func ObtenerColonias() (*[]Colonias, error) {
	db := GetDB()

	var colonia []Colonias

	if err := db.Find(&colonia).Error;
		gorm.IsRecordNotFoundError(err) {
		return nil, errors.New("No se encuentra la colonia")
	} else if err != nil {
		return nil, err
	}
	return &colonia, nil
}


func ObtenerColoniasVotos(limit, pagina int, consultas []Consulta) (*[]VotosPartidoColonia, error) {

	db := GetDB()
	var partidos []VotosPartidoColonia

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&partidos)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&partidos).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return &partidos, nil
}

func FindColoniasFiltradas(limit, pagina int, consultas []Consulta) (*[]Colonias, error){


	db := GetDB()
	var colonias []Colonias

	if consultas != nil && !MoGeneral.EstaVacio(consultas) {
		db = BuildQuery(db, consultas)
	}

	db.Find(&colonias)

	if err := db.Offset((pagina - 1) * limit).Limit(limit).Find(&colonias).Error;
		gorm.IsRecordNotFoundError(err) {
		fmt.Println("Hubo un error")
		return nil, errors.New("No existen registros")
	} else if err != nil {
		fmt.Println("Hubo un error :", err)

		return nil, err
	}

	return &colonias, nil
}