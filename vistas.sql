
create view Lista_coordinadores as (
  select
    p.id             as persona_id,
    p.nombre,
    p.app,
    p.apm,
    p.clave_elec     as clave_elec,
    p.seccion_id,
    t.descripcion    as tipo,
    pt.id            as partido_id,
    pt.nombre        as partido,
    pt.abreviaciones as abreviacion
  from personas p inner join persona_coordinador c on p.id = c.persona_id
    inner join tipo_coordinador t on t.id = c.tipo_coordinador_id
    left join partidos pt on pt.id = c.partido_id);

create view Lista_coordinadores_seccion as
  (
    select
      p.id,
      p.nombre         as nombre,
      p.app            as app,
      p.apm            as apm,
      p.clave_elec     as clave_elec,
      g.id             as grupo_id,
      g.clave          as grupo_clave,
      e.id             as estructura_id,
      e.nombre         as estructura_nombre,
      cs.seccion_id    as seccion_id,
      p2.id            as partido_id,
      p2.nombre        as partido_nombre,
      p2.abreviaciones as partido_abreviacion
    from coordinador_secciones cs inner join personas p on cs.personaseccion_id = p.id
      inner join grupos g on cs.grupo_id = g.id
      inner join estructuras e on g.estructura_id = e.id
      inner join partidos p2 on g.partido_id = p2.id
  );

create view Lista_coordinadores_manzana as
  (
    SELECT
      dd.personamanzana_id         as id,
      dd.personamanzana_nombre     as nombre,
      dd.personamanzana_app        as app,
      dd.personamanzana_apm        as apm,
      dd.personamanzana_seccion_id as seccion_id,
      dd.personamanzana_clave_elec as clave_elec,
      dd.grupo_id,
      dd.estructura_id,
      dd.estructura_nombre,
      dd.partido_id,
      dd.partido                   as partido_nombre,
      dd.abreviacion               as partido_abreviacion,
      pp.id                        as personaseccion_id,
      pp.nombre                    as personaseccion_nombre,
      pp.app                       as personaseccion_app,
      pp.apm                       as personaseccion_apm
    FROM
      (
        SELECT
          P.ID                 AS personamanzana_id,
          P.nombre             as personamanzana_nombre,
          P.app                as personamanzana_app,
          P.apm                as personamanzana_apm,
          P.clave_elec         as personamanzana_clave_elec,
          cm.seccion_id        as personamanzana_seccion_id,
          g.id                 as grupo_id,
          e.id                 as estructura_id,
          e.nombre             as estructura_nombre,
          p2.ID                AS partido_id,
          p2.nombre            AS partido,
          p2.abreviaciones     AS abreviacion,
          cm.personaseccion_id AS personaseccion_id
        FROM
          coordinador_manzanas cm
          INNER JOIN personas P ON cm.personamanzana_id = P.
        ID
          INNER JOIN grupos G ON cm.grupo_id = G.
        ID
          inner join estructuras e on g.estructura_id = e.id

          INNER JOIN partidos p2 ON G.partido_id = p2.ID
      ) AS dd
      INNER JOIN personas pp ON dd.personaseccion_id = pp.ID
  );

create view Lista_promotores as
  (select
     dd.personapromotor_id     as id,
     dd.personapromotor_nombre as nombre,
     dd.personapromotor_app    as app,
     dd.personapromotor_apm    as apm,
     dd.clave_elec             as clave_elec,
     dd.partido_id,
     dd.partido_nombre,
     dd.partido_abreviacion,
     dd.grupo_id,
     dd.estructura_id,
     dd.estructura_nombre,
     dd.seccion_id,
     dd.personaseccion_id,
     dd.personamanzana_id,
     pp.nombre                 as personamanzana_nombre,
     pp.app                    as personamanzana_app,
     pp.apm                    as personamanzana_apm
   from
     (select
        p.personapromotor_id,
        p2.nombre        as personapromotor_nombre,
        p2.app           as personapromotor_app,
        p2.apm           as personapromotor_apm,
        p2.clave_elec,
        p.grupo_id,
        p.seccion_id,
        p.personaseccion_id,
        g.partido_id     as partido_id,
        e.id             as estructura_id,
        e.nombre         as estructura_nombre,
        p3.nombre        as partido_nombre,
        p3.abreviaciones as partido_abreviacion,
        p.personamanzana_id
      from promotores p inner join personas p2 on p.personapromotor_id = p2.id
        inner join coordinador_manzanas cm on p.personamanzana_id = cm.personamanzana_id
        inner join grupos g on g.id = p.grupo_id
        inner join estructuras e on g.estructura_id = e.id

        inner join partidos p3 on g.partido_id = p3.id) as dd inner join personas as pp
       on dd.personamanzana_id = pp.id);

create view Lista_promovidos as (
  select
    dd.personavotante_id     as id,
    dd.personavotante_nombre as nombre,
    dd.personavotante_app    as app,
    dd.personavotante_apm    as apm,
    dd.clave_elec            as clave_elec,
    dd.grupo_id,
    dd.estructura_id,
    dd.estructura_nombre,
    dd.seccion_id,
    dd.personaseccion_id,
    dd.personamanzana_id,
    dd.partido_id,
    dd.partido_nombre,
    dd.partido_abreviacion,
    dd.personapromotor_id,
    pp.nombre                as personapromotor_nombre,
    pp.app                   as personapromotor_app,
    pp.apm                   as personapromotor_apm
  from (
         select
           v.personavotante_id,
           p.nombre         as personavotante_nombre,
           p.app            as personavotante_app,
           p.apm            as personavotante_apm,
           p.clave_elec     as clave_elec,
           v.grupo_id,
           e.id             as estructura_id,
           e.nombre         as estructura_nombre,
           v.seccion_id,
           v.personaseccion_id,
           v.personamanzana_id,
           p2.id            as partido_id,
           p2.nombre        as partido_nombre,
           p2.abreviaciones as partido_abreviacion,
           v.personapromotor_id
         from votantes v inner join personas p on v.personavotante_id = p.id
           inner join grupos g on v.grupo_id = g.id
           inner join estructuras e on g.estructura_id = e.id

           inner join partidos p2 on g.partido_id = p2.id) as dd inner join personas pp on pp.id = dd.personapromotor_id

);

create view reporte_total_distrito as (
  SELECT *
  FROM
    (
      SELECT
        dd.distrito,
        dd.tipo,
        dd.abreviacion    AS partido,
        COUNT(persona_id) AS total
      FROM
        (
          SELECT
            lc.persona_id,
            lc.nombre,
            lc.app,
            lc.apm,
            lc.tipo,
            lc.seccion_id,
            lc.abreviacion,
            s.distrito
          FROM
            lista_coordinadores lc
            INNER JOIN secciones s ON lc.seccion_id = s.ID
          ORDER BY
            lc.persona_id
        ) AS dd
      GROUP BY
        dd.abreviacion,
        dd.tipo,
        dd.distrito
      ORDER BY
        dd.abreviacion,
        dd.distrito,
        dd.tipo
    ) AS cc
  UNION
  (
    SELECT
      s.distrito,
      'PROMOVIDOS'           AS tipo,
      lp.partido_abreviacion AS partido,
      COUNT(lp.ID)              total
    FROM
      lista_promovidos lp
      INNER JOIN secciones s ON lp.seccion_id = s.ID
    GROUP BY
      s.distrito,
      lp.partido_abreviacion
  )
  ORDER BY
    partido,
    distrito,
    tipo);

--total por distrito,seccion,tipo,partido por seccion (direccion)
CREATE VIEW reporte_total_distrito_seccion as (
  SELECT
    dd.distrito,
    dd.seccion_id     as seccion,
    dd.tipo,
    dd.abreviacion    AS partido,
    COUNT(persona_id) AS total
  FROM
    (
      SELECT
        lc.persona_id,
        lc.nombre,
        lc.app,
        lc.apm,
        lc.tipo,
        lc.seccion_id,
        lc.abreviacion,
        s.distrito
      FROM
        lista_coordinadores lc
        INNER JOIN secciones s ON lc.seccion_id = s.ID
      ORDER BY
        lc.persona_id
    ) AS dd
  GROUP BY
    dd.abreviacion,
    dd.tipo,
    dd.distrito,
    dd.seccion_id
  ORDER BY
    dd.abreviacion,
    dd.distrito,
    dd.seccion_id,
    dd.tipo);

create view lista_promovidos_repetidos as
  (select
     lp.nombre,
     lp.app,
     lp.apm,
     lp.seccion_id,
     lp.partido_abreviacion,
     lp.personapromotor_nombre,
     lp.personapromotor_app,
     lp.personapromotor_apm
   from
     (select
        id,
        count(id) total
      from lista_promovidos
      GROUP BY id) as lpr
     inner join lista_promovidos lp
       on lpr.id = lp.id
   where lpr.total > 1);

CREATE OR REPLACE VIEW concentrado_total AS
  SELECT
    dd.tipo,
    dd.total
  FROM (SELECT
          cc.tipo,
          sum(cc.total) AS total
        FROM (SELECT
                dd_1.seccion,
                dd_1.tipo,

                count(dd_1.tipo) AS total
              FROM (SELECT
                      lc.persona_id,
                      lc.nombre,
                      lc.app,
                      lc.apm,
                      lc.clave_elec,
                      lc.tipo,
                      s.id AS seccion,
                      s.tipo_seccion,
                      s.distrito
                    FROM lista_coordinadores lc
                      JOIN secciones s ON lc.seccion_id = s.id) dd_1
              GROUP BY dd_1.seccion, dd_1.tipo
              ORDER BY dd_1.seccion) cc
        GROUP BY cc.tipo
        ORDER BY cc.tipo) dd
  UNION
  SELECT
    'PROMOVIDOS' :: text AS tipo,
    count(lp.id)         AS total
  FROM lista_promovidos lp
    JOIN secciones s ON lp.seccion_id = s.id
  ORDER BY tipo;

create view Lista_representantes as (
  select
    p.id ,
    p.nombre,
    p.app,
    p.apm,
    t.descripcion as tipo
  from personas p inner join persona_representante r on p.id = r.persona_id
    inner join tipo_coordinador t on t.id = r.tipo_representante_id);

create view votos_por_partido as (
  select
    p.abreviaciones as partido,
    total
  from
    (select
       partido_id,
       sum(numero_votos) total
     from votos
     group by partido_id) as ptotal
    inner join partidos p on ptotal.partido_id = p.id
    inner join personas per on p.persona_id = per.id
);

create view total_votos_candidato as (
  select
    per.nombre,
    per.app,
    per.apm,
    tcandidato.total
  from
    (select
       persona_id,
       sum(numero_votos) total
     from votos vt
       inner join partidos pt on vt.partido_id = pt.id
     group by persona_id) as tcandidato
    inner join personas per on tcandidato.persona_id = per.id
);

create view promovidos_por_estructura as (
  select
    E.nombre,
    count(V.personavotante_id) as total
  from votantes V
    inner join grupos G
      on V.grupo_id = G.id
    inner join estructuras E
      on G.estructura_id = E.id
  group by E.nombre
);

--1.- que funcionario se cambiaron y/o por quien se reemplazo
create view funcionario_casilla_historial as (
  select
    fh.casilla_id,
    fh.seccion_id,
    fh.rol,
    pp.nombre     nombre_func_nuevo,
    pp.app        app_func_nuevo,
    pp.apm        apm_func_nuevo,
    pp.clave_elec clave_elect_func_nuevo,
    p.nombre      nombre_func_anterior,
    p.app         app_func_anterior,
    p.apm         apm_func_anterior,
    p.clave_elec  clave_elec_func_anterior
  from funcionarios_historial fh inner join personas p on fh.persona_id = p.id
    inner join funcionarios f on fh.casilla_id = f.casilla_id and fh.seccion_id = f.seccion_id and fh.rol = f.rol
    inner join personas pp on f.persona_id = pp.id);
--2
create view casilla_direccion_historial as (
  select
    cd.casilla_id,
    cd.seccion_id,
    d.calle,
    d.colonia,
    d.codigo_postal
  from casilla_direccion cd
    inner join casillas c on cd.casilla_id = c.id and cd.seccion_id = c.seccion_id
    inner join direcciones d on c.direccion_id = d.id);

--3.- reporte promovidos por estructura&sección
create view promovidos_por_estructura_seccion as (
  select
    estructura_nombre estructura,
    seccion_id        seccion,
    count(id)         total
  from lista_promovidos
  group by estructura_nombre, seccion_id
);

--4.- reporte de casillas con incidencias
create view casillas_con_incidencias as (
  select
    ci.casilla_id,
    ci.seccion_id,
    d.calle,
    d.colonia,
    d.codigo_postal,
    i.descripcion
  from casilla_incidencia ci
    inner join casillas c on ci.casilla_id = c.id and ci.seccion_id = c.seccion_id
    inner join incidencias i on ci.incidencia_id = i.id
    inner join direcciones d on c.direccion_id = d.id);

--5
create view casillas_cambio_direccion as (
  select
    cd.casilla_id,
    cd.seccion_id,
    d.calle,
    d.colonia,
    d.codigo_postal
  from casilla_direccion cd
    inner join casillas c on cd.casilla_id = c.id and cd.seccion_id = c.seccion_id
    inner join direcciones d on c.direccion_id = d.id);

--6.- listado de coordinadores de manzana, promotores y promovidos por sección (para lunes)
create view coordinador_mpp_seccion as (
  select
    lp.personamanzana_nombre nombre_coord_manzana,
    lp.personamanzana_app    nombre_app_coord_manzana,
    lp.personamanzana_apm    nombre_apm_coord_manzana,
    lp.nombre                nombre_promotor,
    lp.app                   nombre_app,
    lp.apm                   nombre_apm,
    lp.clave_elec,
    lpr.nombre               nombre_promovido,
    lpr.app                  nombre_app_promovido,
    lpr.apm                  nombre_apm_promovido,
    lpr.clave_elec           clave_elec_promovido,
    lpr.estructura_nombre,
    lpr.seccion_id,
    lp.partido_abreviacion


  from lista_promotores lp inner join lista_promovidos lpr on lp.id like lpr.personapromotor_id
  order by lp.seccion_id, lp.personamanzana_nombre, lp.personamanzana_app, lp.personamanzana_apm);
