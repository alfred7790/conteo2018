package PersonaController

import (
	"github.com/gin-gonic/gin"
	"fmt"
	"net/http"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Session"
	"gopkg.in/mgo.v2/bson"
	"time"
	"strings"
)

func CrearPersona(ctx *gin.Context) {
	var persona Entidades.Personas
	err := ctx.BindJSON(&persona)
	if err != nil {
		fmt.Println("Error al pasar la estructura")
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}
	persona.Nombre = strings.ToUpper(persona.Nombre)
	persona.App = strings.ToUpper(persona.App)
	if persona.Apm != "" {
		persona.Apm = strings.ToUpper(persona.Apm)
	}

	if persona.ClaveElec != "" {
		persona.ClaveElec = strings.ToUpper(persona.ClaveElec)
	}

	if persona.Curp != "" {
		persona.Curp = strings.ToUpper(persona.Curp)
	}

	persona.Agregado = true

	persona.Id = bson.NewObjectId().Hex()
	persona.Direccion.Id = bson.NewObjectId().Hex()
	for i := range persona.Telefono {
		if persona.Telefono[i].Id == "" {
			persona.Telefono[i].Id = bson.NewObjectId().Hex()
		}
	}

	if persona.ClaveElec != "" {
		err := Entidades.ValidarClaveElector(persona.ClaveElec)
		if err != nil {
			MoGeneral.Responder(http.StatusBadRequest, err.Error(), "Ya existe esta clave de elector", ctx)
			return
		}
	}

	if persona.Curp != "" {
		err := Entidades.ValidarCurp(persona.Curp)
		if err != nil {
			MoGeneral.Responder(http.StatusBadRequest, err.Error(), "Ya existe esta curp", ctx)
			return
		}
	}

	tx := Entidades.IniciarTransaccion()

	p, err := Entidades.CrearPersona(persona, tx)
	if err != nil {
		fmt.Println("Error al crear persona")
		tx.Rollback()
		MoGeneral.Responder(http.StatusBadRequest, "No se pudo agregar la persona, tal vez la seccion no exista", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 2
	movimiento.Extra = p.Id
	movimiento.Observacion = "Agrego a la persona con ese id"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah agregado correctamente", p, ctx)
	return
}

func ObtenerPersonas(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)
	personas, err := Entidades.FindAllPersonas(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", personas, ctx)
	return
}

func ActualizarPersona(ctx *gin.Context) {
	var persona Entidades.Personas

	err := ctx.BindJSON(&persona)
	if err != nil {
		//	fmt.Println("Error al pasar la estructura")
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	for i := range persona.Telefono {
		if persona.Telefono[i].Id == "" {
			persona.Telefono[i].Id = bson.NewObjectId().Hex()
		}
	}

	if persona.Id == "" || persona.SeccionId == "" || persona.Nombre == "" {
		MoGeneral.Responder(http.StatusBadRequest, "La seccion, nombre y apellido paterno son obligatorios", "", ctx)
		return
	}

	if persona.Apm != "" {
		persona.Apm = strings.ToUpper(persona.Apm)
	}

	if persona.ClaveElec != "" {
		persona.ClaveElec = strings.ToUpper(persona.ClaveElec)
	}

	if persona.Curp != "" {
		persona.Curp = strings.ToUpper(persona.Curp)
	}

	tx := Entidades.IniciarTransaccion()
	p, err := Entidades.ActualizarPersona(persona, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al actualizar datos", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 3
	movimiento.Extra = p.Id
	movimiento.Observacion = "Actualizo a la persona con ese id"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		//	fmt.Println("Error al insertar movimiento")
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}
	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Datos actualizados correctamente", p, ctx)
	return
}


func EliminarTelefono(ctx *gin.Context) {
	var telefono Entidades.Telefonos

	err := ctx.BindJSON(&telefono)
	if err != nil {
		//fmt.Println("Error al pasar la estructura")
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	if telefono.Id == "" {
		MoGeneral.Responder(http.StatusBadRequest, "No se puede eliminar un telefono sin identificador", "Telefono sin identificador", ctx)
		return
	} else {

		tx := Entidades.IniciarTransaccion()
		err := Entidades.EliminarTelefono(telefono, tx)
		if err != nil {
			//	fmt.Println("Error al eliminar")
			tx.Rollback()
			MoGeneral.Responder(http.StatusInternalServerError, "Problema al eliminar", []string{err.Error()}, ctx)
			return
		}
		tx.Commit()
		MoGeneral.Responder(http.StatusOK, "El telefono se ah eliminado", "", ctx)
		return
	}
}

func AgregarCoordinador(ctx *gin.Context) {
	var rol Entidades.PersonaCoordinador

	err := ctx.BindJSON(&rol)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	if rol.Meta<0 {
		MoGeneral.Responder(http.StatusBadRequest, "Inserte una meta real", "", ctx)
		return
	}

	if rol.TipoCoordinadorId == 4 {
		if rol.PartidoId == nil || *rol.PartidoId == "" {
			MoGeneral.Responder(http.StatusBadRequest, "Asigna un partido al coordinador", "", ctx)
			return
		}
	}

	tx := Entidades.IniciarTransaccion()
	p, err := Entidades.AgregarCoordinador(rol, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusBadRequest, "Ya esta asignado como coordinador, eliminelo antes de reasignarlo", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 1
	movimiento.Extra = rol.PersonaId
	movimiento.Observacion = "Agrego el coordinador "

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}
	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Agregado correctamente", p, ctx)
	return
}

func ValidarCoordinador(ctx *gin.Context) {
	var coordinador Entidades.PersonaCoordinador

	err := ctx.BindJSON(&coordinador)
	if err != nil {
		fmt.Println("Error al pasar la estructura")
		MoGeneral.Responder(http.StatusConflict, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	if coordinador.PersonaId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "No se puede eliminar, si no es coordinador", "No existe el id", ctx)
		return
	} else {

		c, err := Entidades.BuscarPersonaCoordinador(coordinador.PersonaId)
		if err != nil {
			MoGeneral.Responder(http.StatusBadRequest, err.Error(), "", ctx)
			return
		}

		switch c.TipoCoordinadorId {
		case 4:
			err = Entidades.BuscarEnCoordinadoresZona(coordinador.PersonaId)
			if err != nil {
				MoGeneral.Responder(http.StatusBadRequest, err.Error(), "", ctx)
				return
			}
			EliminarCoordinador(coordinador, ctx)
			return
		case 3:
			err = Entidades.BuscarEnCoordinadoresSeccion(coordinador.PersonaId)
			if err != nil {
				MoGeneral.Responder(http.StatusBadRequest, err.Error(), "", ctx)
				return
			}
			EliminarCoordinador(coordinador, ctx)
			return
		case 2:
			err = Entidades.BuscarEnCoordinadoresManzana(coordinador.PersonaId)
			if err != nil {
				MoGeneral.Responder(http.StatusBadRequest, err.Error(), "", ctx)
				return
			}
			EliminarCoordinador(coordinador, ctx)
			return
		case 1:
			err = Entidades.BuscarEnPromotores(coordinador.PersonaId)
			if err != nil {
				MoGeneral.Responder(http.StatusBadRequest, err.Error(), "", ctx)
				return
			}
			EliminarCoordinador(coordinador, ctx)
			return
		case 8:
			err = Entidades.BuscarEnGrupos(coordinador.PersonaId)
			if err != nil {
				MoGeneral.Responder(http.StatusBadRequest, err.Error(), "", ctx)
				return
			}
			EliminarCoordinador(coordinador, ctx)
			return
		default:
			fmt.Println("Entro al default")
			MoGeneral.Responder(http.StatusBadRequest, "No es un coordinador válido", "", ctx)
			return
		}
	}

}

/*	tx := Entidades.IniciarTransaccion()
	persona, err := Entidades.EliminarCoordinador(coordinador, tx)
	if err != nil {
		//fmt.Println("Error al eliminar")
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al eliminar", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 4
	movimiento.Extra = coordinador.PersonaId
	movimiento.Observacion = "Elimino el rol de coordinador "

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, nil)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah eliminado su rol de coordinador", persona, ctx)
	return*/

func EliminarCoordinador(coordinador Entidades.PersonaCoordinador, ctx *gin.Context) {
	tx := Entidades.IniciarTransaccion()
	persona, err := Entidades.EliminarCoordinador(coordinador, tx)
	if err != nil {
		//fmt.Println("Error al eliminar")
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al eliminar", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 4
	movimiento.Extra = coordinador.PersonaId
	movimiento.Observacion = "Elimino el rol de coordinador "

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah eliminado su rol de coordinador", persona, ctx)
	return
}

func ActualizarCoordinador(ctx *gin.Context) {
	var coordinador Entidades.PersonaCoordinador

	err := ctx.BindJSON(&coordinador)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	if coordinador.Meta < 0 {
		MoGeneral.Responder(http.StatusBadRequest, "Asigna un valor válido", "", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()
	coor, err := Entidades.ActualizarCoordinador(coordinador, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusBadRequest, err.Error(), "", ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 6
	movimiento.Extra = coor.PersonaId
	movimiento.Observacion = "Actualizo los datos  como coordinador"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se actualizo correctamente", coor, ctx)
	return
}

func ObtenerCoordinadores(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)
	personas, err := Entidades.FindListaCoordinadores(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}
	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", personas, ctx)
	return
}

func VotoYa(ctx *gin.Context) {
	personaId := ctx.Query("personaId")
	if personaId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "Asigne un id valido", "", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	err := Entidades.RegistrarVoto(personaId, tx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema registrar voto", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 32
	movimiento.Extra = personaId
	movimiento.Observacion = "Registro de voto de una persona"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se registro el voto", "", ctx)
	return
}
