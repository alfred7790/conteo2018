package PersonaController

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Session"
	"fmt"
)

func AgregarRepresentante(ctx *gin.Context) {
	var representante Entidades.PersonaRepresentante

	err := ctx.BindJSON(&representante)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()
	p, err := Entidades.AgregarRepresentante(representante, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusBadRequest, "Ya esta asignado un tipo de representante, eliminelo antes de reasignarlo", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 10
	movimiento.Extra = representante.PersonaId
	movimiento.Observacion = "Agrego el representante "

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusNotAcceptable, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}
	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Agregado correctamente", p, ctx)
	return
}

func EliminarRepresentante(ctx *gin.Context) {
	var representante Entidades.PersonaRepresentante

	err := ctx.BindJSON(&representante)
	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}
	if representante.PersonaId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "No se puede eliminar, si no es representante", "No existe el id", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	persona, err := Entidades.EliminarRepresentante(representante, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al eliminar", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 11
	movimiento.Extra = representante.PersonaId
	movimiento.Observacion = "Elimino el rol de representante"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		fmt.Print(err)
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", err.Error(), ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah eliminado su rol de coordinador", persona, ctx)
	return

}

func ObtenerRepresentantes(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)
	personas, err := Entidades.FindListaRepresentantes(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", personas, ctx)
	return
}
