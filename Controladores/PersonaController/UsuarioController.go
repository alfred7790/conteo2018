package PersonaController

import (
	"github.com/gin-gonic/gin"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Cifrado"
	"net/http"
)

func CrearUsuario(ctx *gin.Context) {
	var Usuario Entidades.Usuarios
	err := ctx.BindJSON(&Usuario)
	if err != nil {
		//	fmt.Println("Error al pasar la estructura")
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	if Usuario.Password == "" || Usuario.Usuario == "" {
		//	fmt.Println("Error al pasar la estructura")
		MoGeneral.Responder(http.StatusBadRequest, "No se puede crear el usuario con datos vacios", "Datos incorrectoa", ctx)
		return
	}

	Usuario.Password = Cifrado.Cryp(Usuario.Password)

	tx := Entidades.IniciarTransaccion()

	p, err := Entidades.CrearUsuario(Usuario, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al crear usuario", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Creada correctamente", p, ctx)
	return
}

func ObtenerUsuarios(ctx *gin.Context) {
	usuarios, err := Entidades.ObtenerUsuarios()
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al obtener usuarios", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Resultados obtenidos",usuarios, ctx)
	return
}
