package PartidoController

import (
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Session"
	"net/http"
	"gopkg.in/mgo.v2/bson"
	"time"
	"github.com/gin-gonic/gin"
)

func CrearPartido2(ctx *gin.Context) {
	var partido Entidades.Partidos2
	err := ctx.BindJSON(&partido)

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al crear partido", err.Error(), ctx)
		return
	}

	partido.Id = bson.NewObjectId().Hex()

	tx := Entidades.IniciarTransaccion()

	s, err := Entidades.CrearPartido2(partido, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusConflict, "Problema al crear partido", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 9
	movimiento.Extra = partido.Id
	movimiento.Observacion = "Se creo el partido"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se agrego el partido correctamente", s, ctx)
	return
}

func ObtenerPartidos2(ctx *gin.Context){
	partidos,err := Entidades.ObtenerPartidos2()

	if err!= nil{
		MoGeneral.Responder(http.StatusConflict, "Problema al obtener partidos", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Resultados obtenidos", partidos, ctx)
	return
}

func FindPartidos2(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)
	personas, err := Entidades.FindAllPartidos2(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}
	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", personas, ctx)
	return
}
