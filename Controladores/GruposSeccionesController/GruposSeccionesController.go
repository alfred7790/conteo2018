package GruposSeccionesController

import (
	"github.com/gin-gonic/gin"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Session"
	"net/http"
	"gopkg.in/mgo.v2/bson"
	"time"
)

func CrearGrupoSeccion(ctx *gin.Context){
	var grupo Entidades.Grupos
	err := ctx.BindJSON(&grupo)

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al crear grupo", err.Error(), ctx)
		return
	}

	grupo.Id = bson.NewObjectId().Hex()

	tx := Entidades.IniciarTransaccion()

	s, err := Entidades.CrearGrupo(grupo, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusConflict, "Es posible que la clave ya este registrada", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 7
	movimiento.Extra = grupo.Id
	movimiento.Observacion = "Se creo el grupo"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se agrego el grupo correctamente", s, ctx)
	return
}