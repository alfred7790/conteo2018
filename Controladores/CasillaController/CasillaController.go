package CasillaController

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Session"
	"time"
	"gopkg.in/mgo.v2/bson"
	"strconv"
	"fmt"
)

func CrearCasilla(ctx *gin.Context) {
	var casilla Entidades.Casillas
	err := ctx.BindJSON(&casilla)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al crear la casilla", err.Error(), ctx)
		return
	}

	if casilla.Id == "" || casilla.SeccionId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "Completa todos los campos", "", ctx)
		return
	}

	casilla.Direccion.Id = bson.NewObjectId().Hex()
	tx := Entidades.IniciarTransaccion()
	p, err := Entidades.CrearCasilla(casilla, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "No se puede crear la casilla, posiblemente ya este registrada", err.Error(), ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 16
	movimiento.Extra = casilla.Id
	movimiento.Extra2 = casilla.SeccionId
	movimiento.Observacion = "Se creo la casilla"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Creada correctamente", p, ctx)
	return
}

func EliminarCasilla(ctx *gin.Context) {
	var casilla Entidades.Casillas

	err := ctx.BindJSON(&casilla)
	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	if casilla.Id == "" || casilla.SeccionId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "No se puede eliminar, si no es casilla", "No existe el id", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	err = Entidades.EliminarCasilla(casilla, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "No se puede eliminar, ya tiene asignado funcionarios o representantes", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 17
	movimiento.Extra = casilla.Id
	movimiento.Extra2 = casilla.SeccionId
	movimiento.Observacion = "Se elimino la casilla"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", err.Error(), ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah eliminado correctamente", "", ctx)
	return

}

/*
func CancelarCasilla(ctx *gin.Context) {
	id := ctx.Query("id")

	if id == "" {
		fmt.Println("Id incorrecto")
		MoGeneral.Responder(http.StatusBadRequest, "Casilla Invalida", "", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()
	s, err := Entidades.CancelarCasilla(id, tx)
	if err != nil {
		fmt.Println("Error: ", err)
		tx.Rollback()
		MoGeneral.Responder(http.StatusConflict, "Problema al eliminar casilla", []string{err.Error()}, ctx)
		return
	}
	tx.Commit()

	MoGeneral.Responder(http.StatusOK, "Cancelada correctamente", s, ctx)
	return
}*/

func ActualizarCasilla(ctx *gin.Context) {
	var casilla Entidades.Casillas

	err := ctx.BindJSON(&casilla)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	if casilla.Id == "" || casilla.SeccionId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "Datos incompletos", "", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()
	coor, err := Entidades.ActualizarCasilla(casilla, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusBadRequest, err.Error(), "", ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 22
	movimiento.Extra = casilla.Id
	movimiento.Extra2 = casilla.SeccionId
	movimiento.Observacion = "Actualizo los datos de la casilla"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, nil)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se actualizo correctamente", coor, ctx)
	return
}

func GenerarIncidencias(ctx *gin.Context) {
	var incidencias []Entidades.CasillaIncidencia
	err := ctx.BindJSON(&incidencias)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al crear la casilla", err.Error(), ctx)
		return
	}

	if len(incidencias) == 0 {
		MoGeneral.Responder(http.StatusBadRequest, "Registra al menos una incidencia", "", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	p, err := Entidades.RegistrarIncidencias(incidencias, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "No se puede crear la casilla, posiblemente ya este registrada", err.Error(), ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 30
	movimiento.Extra = strconv.Itoa(len(incidencias))
	movimiento.Observacion = "Se registraron incidencias"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Creada correctamente", p, ctx)
	return
}

func RegistrarVotos(ctx *gin.Context) {
	var votos Entidades.Votos
	err := ctx.BindJSON(&votos)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al registrar votos", []string{err.Error()}, ctx)
		return
	}

	if votos.NumeroVotos <= 0 {
		MoGeneral.Responder(http.StatusBadRequest, "Registre un numero de votos validos", "", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()
	p, err := Entidades.RegistrarVotos(votos, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar votos", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Registrados correctamente", p, ctx)
	return
}

func RegistrarVotosxPartidos(ctx *gin.Context) {
	var votos []Entidades.Votos

	err := ctx.BindJSON(&votos)

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al registrar votos", []string{err.Error()}, ctx)
		return
	}

	if len(votos) == 0 {
		MoGeneral.Responder(http.StatusBadRequest, "Registra votos primero", "", ctx)
		return
	}
	var votosTotales, votosNulos int
	tx := Entidades.IniciarTransaccion()
	for i, _ := range votos {
		if votos[i].NumeroVotos < 0 {
			tx.Rollback()
			MoGeneral.Responder(http.StatusBadRequest, "No se pueden registar votos negativos", "", ctx)
			return
		}
		_, err := Entidades.RegistrarVotos(votos[i], tx)
		if err != nil {
			tx.Rollback()
			MoGeneral.Responder(http.StatusConflict, "Ya se encuentran registrados votos para el partido", []string{err.Error()}, ctx)
			return
		}

		votosTotales += votos[i].NumeroVotos
		if votos[i].PartidoId == "5b32d96d91aa7f1905fefaad" {
			votosNulos = votos[i].NumeroVotos
		}
	}

	err = Entidades.ActualizarVotosCasilla(votos[0].CasillaId, votos[0].SeccionId, votosTotales, votosNulos, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusConflict, err.Error(), "", ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 36
	movimiento.Extra = votos[0].SeccionId + "-" + votos[0].CasillaId
	movimiento.Extra2 = strconv.Itoa(votosTotales)
	movimiento.Observacion = "Registro de votos a casilla"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Registrados correctamente", "", ctx)
	return
}

func RegistrarVotosEstimados(ctx *gin.Context) {
	var votos Entidades.VotosEstimados
	err := ctx.BindJSON(&votos)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al registrar votos", []string{err.Error()}, ctx)
		return
	}

	if votos.VotosEstimados <= 0 {
		MoGeneral.Responder(http.StatusBadRequest, "Registre un numero de votos validos", "", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()
	err = Entidades.RegistrarVotosEstimados(votos, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar votos", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 38
	movimiento.Extra = votos.SeccionId + "-" + votos.CasillaId
	movimiento.Extra2 = strconv.Itoa(votos.VotosEstimados)
	movimiento.Observacion = "Se registro una estimacion de votos"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Registrados correctamente", "", ctx)
	return
}

func ActualizarDisponibilidad(ctx *gin.Context) {
	var casilla Entidades.Casillas
	err := ctx.BindJSON(&casilla)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al crear la casilla", err.Error(), ctx)
		return
	}

	if casilla.Id == "" || casilla.SeccionId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "Completa todos los campos", "", ctx)
		return
	}
	perfil := Session.GetProfile(ctx.Request)
	fmt.Println("Perfil ID: ", perfil)
	tx := Entidades.IniciarTransaccion()
	err = Entidades.DeshabilitarCasilla(casilla, perfil, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusBadRequest, err.Error(), "", ctx)
		return
	}

	var extra string
	if casilla.Vigente {
		extra = "Disponible"
	} else {
		extra = "No disponible"
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 39
	movimiento.Extra = casilla.Id + "-" + casilla.SeccionId
	movimiento.Extra2 = extra
	movimiento.Observacion = "Se deshabilito o habilito la casilla"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}
	fmt.Println("Vigencia final: ", casilla.Vigente)
	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Actualizada correctamente", "", ctx)
	return
}

func FindCasillas(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)
	personas, err := Entidades.FindAllCasillas(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}
	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", personas, ctx)
	return
}

func FindIncidencias(ctx *gin.Context) {
	incidencias, err := Entidades.FindIncidencias()

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", err.Error(), ctx)
		return
	}
	MoGeneral.Responder(http.StatusOK, "Se han obtenido incidencias", incidencias, ctx)
	return
}

func FindIncidenciasCasilla(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)

	incidencias, err := Entidades.FindAllIncidenciasCasilla(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", incidencias, ctx)
	return
}

func EliminarIncidencia(ctx *gin.Context) {
	var incidencias Entidades.CasillaIncidencia
	err := ctx.BindJSON(&incidencias)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al registrar votos", []string{err.Error()}, ctx)
		return
	}
	tx := Entidades.IniciarTransaccion()
	err = Entidades.EliminarIncidencia(incidencias, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al eliminar incidencia", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 31
	movimiento.Extra = incidencias.CasillaId + "-" + incidencias.SeccionId
	movimiento.Observacion = "Se elimino una incidencia"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se elimino la incidencia", "", ctx)
	return
}

func ReemplazarDireccionCasilla(ctx *gin.Context) {
	var casilla Entidades.CasillaCambioDireccion
	err := ctx.BindJSON(&casilla)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al crear la casilla", []string{err.Error()}, ctx)
		return
	}

	if casilla.SeccionId == "" || casilla.CasillaId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "Completa todos los campos", "", ctx)
		return
	}

	casilla.Direccion.Id = bson.NewObjectId().Hex()

	tx := Entidades.IniciarTransaccion()

	err = Entidades.RegistrarDireccionCasilla(casilla, tx)

	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusNotAcceptable, "Problema al cambiar la direccion ", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 35
	movimiento.Extra = casilla.SeccionId + "-" + casilla.CasillaId
	movimiento.Extra2 = casilla.Observacion
	movimiento.Observacion = "Se reemplazo la direccion"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Actualizada correctamente", "", ctx)
	return
}

func ActualizarHoraCasilla(ctx *gin.Context) {
	var casilla Entidades.CasillaHora
	err := ctx.BindJSON(&casilla)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al actualizar hora", []string{err.Error()}, ctx)
		return
	}
	tx := Entidades.IniciarTransaccion()
	err = Entidades.HoraCasilla(casilla, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al eliminar incidencia", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 37
	movimiento.Extra = casilla.SeccionId
	movimiento.Extra2 = casilla.CasillaId
	movimiento.Observacion = "Se actualizo la hora de la casilla"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se actualizo la hora de apertura", "", ctx)
	return

}
