package CasillaController

import (
	"github.com/gin-gonic/gin"
	"../../Modelos/Entidades"
	"../../Modulos/General"

	"net/http"
)

func RegistrarVotosxPartidos2(ctx *gin.Context) {
	var votos []Entidades.Votos2

	err := ctx.BindJSON(&votos)

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al registrar votos", []string{err.Error()}, ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()
	for i, _ := range votos {
		if votos[i].NumeroVotos < 0 {
			tx.Rollback()
			MoGeneral.Responder(http.StatusBadRequest, "No se pueden registar votos negativos o mayores a 750", "", ctx)
			return
		}
		_, err := Entidades.RegistrarVotos2(votos[i], tx)
		if err != nil {
			tx.Rollback()
			MoGeneral.Responder(http.StatusConflict, "Ya se encuentran registrados votos para el partido", []string{err.Error()}, ctx)
			return
		}

	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Registrados correctamente", "", ctx)
	return
}

