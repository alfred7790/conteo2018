package RepresentanteController

import (
	"github.com/gin-gonic/gin"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Session"
	"time"

	"net/http"
)

func CrearRepresentante(ctx *gin.Context) {
	var representante Entidades.Representantes
	err := ctx.BindJSON(&representante)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al crear la casilla", []string{err.Error()}, nil)
		return
	}

	if representante.PersonaId == "" || representante.CasillaId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "Completa todos los campos", "", nil)
		return
	}

	tx := Entidades.IniciarTransaccion()
	p, err := Entidades.CrearRepresentante(representante, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusNotAcceptable, "Este representante ya se encuentra asignado", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 21
	movimiento.Extra = representante.PersonaId
	movimiento.Extra2 = representante.PartidoId
	movimiento.Observacion = "Se creo el representante"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, nil)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Creada correctamente", p, ctx)
	return
}

func EliminarRepresentante(ctx *gin.Context) {
	var representante Entidades.Representantes

	err := ctx.BindJSON(&representante)
	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	if representante.PersonaId == "" || representante.SeccionId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "Datos incompletos", "Campos incompletos", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	err = Entidades.EliminarRepresentanteCasilla(representante, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al eliminar", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 25
	movimiento.Extra = representante.PersonaId
	movimiento.Extra2 = representante.PartidoId
	movimiento.Observacion = "Se elimino el representante de la seccion "+representante.SeccionId

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", err.Error(), nil)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah eliminado correctamente", "", ctx)
	return

}

func ObtenerRepresentantesFiltrados(ctx *gin.Context){
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)

	funcionarios, err := Entidades.FindRepresentantes(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", funcionarios, ctx)
	return
}