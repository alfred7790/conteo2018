package CoordinadorController

import (
	"github.com/gin-gonic/gin"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"net/http"
	"../../Modulos/Session"
	"time"
)

func RegistroCoordinadorManzana(ctx *gin.Context) {
	var coordinador Entidades.CoordinadorManzanas

	err := ctx.BindJSON(&coordinador)

	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al recibir datos", err.Error(), ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	c, err := Entidades.RegistrarCoordinadorManzana(coordinador, tx)

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Ya se encuentra asignado este coordinador de manzana", err.Error(), ctx)
		tx.Rollback()
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 23
	movimiento.Extra = c.PersonamanzanaId
	movimiento.Extra2 = c.PersonaseccionId
	movimiento.Observacion = "Registro el coordinador de manzana "

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah registrado correctamente", "", ctx)
	return
}

func EliminarCoordinadorManzana(ctx *gin.Context) {
	var coordinador Entidades.CoordinadorManzanas

	err := ctx.BindJSON(&coordinador)
	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}
	MoGeneral.CheckStruct(coordinador)
	if coordinador.PersonamanzanaId == "" || coordinador.PersonaseccionId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "No se puede eliminar, si no es coordinador", "No existe el id", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	err = Entidades.EliminarCoordinadorManzana(coordinador, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusBadRequest, "Este coordinador tiene asignado algun promotor", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 24
	movimiento.Extra = coordinador.PersonamanzanaId
	movimiento.Extra2 = coordinador.GrupoId
	movimiento.Observacion = "Se elimino el coordinador de manzana"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", err.Error(), ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah eliminado correctamente", "", ctx)
	return

}

func FindListaCoordinadoresManzana(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)
	personas, err := Entidades.FindListaCoordinadoresManzana(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}
	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", personas, ctx)
	return
}
