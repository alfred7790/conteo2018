package CoordinadorController

import (
	"github.com/gin-gonic/gin"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Session"
	"net/http"
	"time"
)

func RegistroCoordinadorZona(ctx *gin.Context) {
	var coordinador Entidades.CoordinadorZonas

	err := ctx.BindJSON(&coordinador)

	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al recibir datos", err.Error(), ctx)
		return
	}
	MoGeneral.CheckStruct(coordinador)
	tx := Entidades.IniciarTransaccion()

	c, err := Entidades.RegistrarCoordinadorZona(coordinador, tx)

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Este grupo ya tiene asignado a este coordinador", err.Error(), ctx)
		tx.Rollback()
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 12
	movimiento.Extra = c.PersonazonaId
	movimiento.Extra2 = c.GrupoId
	movimiento.Observacion = "Registro el coordinador de zona "

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah registrado correctamente", "", ctx)
	return
}

func EliminarCoordinadorZona(ctx *gin.Context) {
	var coordinador Entidades.CoordinadorZonas

	err := ctx.BindJSON(&coordinador)
	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}
	if coordinador.PersonazonaId == "" || coordinador.GrupoId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "No se puede eliminar, si no es coordinador", "No existe el id", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	err = Entidades.EliminarCoordinadorZona(coordinador, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al eliminar", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 13
	movimiento.Extra = coordinador.PersonazonaId
	movimiento.Extra2 = coordinador.GrupoId
	movimiento.Observacion = "Se elimino la relacion del coordinador de zona"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", err.Error(), ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah eliminado correctamente", "", ctx)
	return

}

func ObtenerCoordinadoresZona(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)

	grupos, err := Entidades.FindAllCoordinadoresZona(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", grupos, ctx)
	return
}

func RegistroCoordinadorSeccion(ctx *gin.Context) {
	var coordinador Entidades.CoordinadorSecciones

	err := ctx.BindJSON(&coordinador)

	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al recibir datos", err.Error(), ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	c, err := Entidades.RegistrarCoordinadorSeccion(coordinador, tx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "La seccion o el coordinador ya han sido asignados a un grupo", err.Error(), ctx)
		tx.Rollback()
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 14
	movimiento.Extra = c.PersonaseccionId
	movimiento.Extra2 = c.GrupoId
	movimiento.Observacion = "Registro el coordinador de seccion "

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah registrado correctamente", "", ctx)
	return
}

func EliminarCoordinadorSeccion(ctx *gin.Context) {
	var coordinador Entidades.CoordinadorSecciones

	err := ctx.BindJSON(&coordinador)
	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}
	if coordinador.PersonaseccionId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "No se puede eliminar, si no es coordinador", "No existe el id", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	err = Entidades.EliminarCoordinadorSeccion(coordinador, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusBadRequest, "Este promotor tiene asignado algun coordinador de manzana", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 15
	movimiento.Extra = coordinador.PersonaseccionId
	movimiento.Extra2 = coordinador.GrupoId
	movimiento.Observacion = "Se elimino la relacion del coordinador de seccion"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", err.Error(), ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah eliminado correctamente", "", ctx)
	return

}

func ObtenerCoordinadoresSeccion(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)

	grupos, err := Entidades.FindAllCoordinadoresSeccion(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", grupos, ctx)
	return
}

func FindListaCoordinadoresSeccion(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)
	personas, err := Entidades.FindListaCoordinadoresSeccion(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}
	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", personas, ctx)
	return
}

func ObtenerRoles(ctx *gin.Context) {
	tipos, err := Entidades.ObtenerRoles()
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Resultados obtenidos", tipos, ctx)
	return
}

