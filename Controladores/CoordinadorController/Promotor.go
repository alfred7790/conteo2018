package CoordinadorController

import (
	"github.com/gin-gonic/gin"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"net/http"
	"../../Modulos/Session"
	"time"
)

func RegistroPromotor(ctx *gin.Context) {
	var promotor Entidades.Promotores

	err := ctx.BindJSON(&promotor)

	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al recibir datos", err.Error(), ctx)
		return
	}
	tx := Entidades.IniciarTransaccion()

	c, err := Entidades.RegistrarPromotor(promotor, tx)

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Ya se encuentra asignado este promotor", err.Error(), ctx)
		tx.Rollback()
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 26
	movimiento.Extra = c.PersonapromotorId
	movimiento.Extra2 = c.PersonaseccionId
	movimiento.Observacion = "Registro de promotor"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah registrado correctamente", "", ctx)
	return
}

func EliminarPromotor(ctx *gin.Context) {
	var promotor Entidades.Promotores

	err := ctx.BindJSON(&promotor)
	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	if promotor.PersonapromotorId == "" || promotor.PersonamanzanaId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "No se puede eliminar, si no es promotor", "No existe el id", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	err = Entidades.EliminarPromotor(promotor, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusBadRequest, "Este promotor tiene asignado algun promovido", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 27
	movimiento.Extra = promotor.PersonapromotorId
	movimiento.Extra2 = promotor.PersonamanzanaId
	movimiento.Observacion = "Se elimino el promotor"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", err.Error(), ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah eliminado correctamente", "", ctx)
	return

}

func FindListaPromotores(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)
	personas, err := Entidades.FindListaPromotores(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}
	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", personas, ctx)
	return
}
