package SeccionController

import (
	"github.com/gin-gonic/gin"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"fmt"
	"net/http"
	"strconv"
	"../../Modulos/Session"
	"time"
)

func CrearSeccion(ctx *gin.Context) {
	var seccion Entidades.Secciones
	err := ctx.BindJSON(&seccion)

	if err != nil {
		fmt.Println("Error al pasar a estructura")
		MoGeneral.Responder(http.StatusBadRequest, "Problema al volver estructura", err.Error(), ctx)
		return
	}

	seccion.Vigente = true

	tx := Entidades.IniciarTransaccion()

	s, err := Entidades.CrearSeccion(seccion, tx)
	if err != nil {
		fmt.Println("Error: ", err)
		tx.Rollback()
		MoGeneral.Responder(http.StatusConflict, "Problema al crear seccion, posiblemente ya exista", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId =20
	movimiento.Extra = seccion.Id
	movimiento.Extra2 = seccion.Distrito
	movimiento.Observacion = "Se creo la seccion"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se creo la seccion correctamente", s, ctx)
	return
}

func BuscarSeccion(ctx *gin.Context) {
	id := ctx.Query("id")
	intid, err := strconv.Atoi(id)
	if err != nil {
		fmt.Println("No se puede convertir a numero", err)
		MoGeneral.Responder(http.StatusConflict, "Problema al hacer la conversion", []string{err.Error()}, ctx)
		return
	}
	seccion, err := Entidades.BuscarSeccion(intid)
	if err != nil {
		fmt.Println("Problema: ", err)
		MoGeneral.Responder(http.StatusConflict, "Problema al buscar la seccion", []string{err.Error()}, ctx)
		return
	}
	MoGeneral.Responder(http.StatusOK, "Encontrada correctamente", seccion, ctx)
	return
}

func ObtenerSecciones(ctx *gin.Context) {
	secciones, err := Entidades.GetAllSeccciones()
	if err != nil {
		fmt.Println("Problema: ", err)
		MoGeneral.Responder(http.StatusConflict, "Problema al buscar la seccion", []string{err.Error()}, ctx)
	}

	MoGeneral.Responder(http.StatusOK, "Resultados encontrados", secciones, ctx)
	return
}

func ObtenerSeccionesFiltradas(ctx *gin.Context){
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)

	secciones, err := Entidades.FindAllSecciones(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", secciones, ctx)
	return
}


func ActualizarSeccion(ctx *gin.Context) {
	var seccion Entidades.Secciones
	err := ctx.BindJSON(&seccion)

	if err != nil {
		fmt.Println("Problema: ", err)
		MoGeneral.Responder(http.StatusConflict, "Problema pasar la estructura", []string{err.Error()}, ctx)
	}

	tx := Entidades.IniciarTransaccion()
	s, err := Entidades.ActualizarSeccion(seccion, tx)
	if err != nil {
		fmt.Println("Error: ", err)
		tx.Rollback()
		MoGeneral.Responder(http.StatusConflict, "Problema al actualizar seccion", []string{err.Error()}, ctx)
		return
	}
	tx.Commit()

	MoGeneral.Responder(http.StatusOK, "Resultados encontrados", s, ctx)
	return
}
