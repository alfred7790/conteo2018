package FuncionarioController

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Session"
)

func CrearFuncionario(ctx *gin.Context) {
	var funcionario Entidades.Funcionarios
	err := ctx.BindJSON(&funcionario)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al crear la casilla", []string{err.Error()}, ctx)
		return
	}

	if funcionario.Rol == "" {
		MoGeneral.Responder(http.StatusBadRequest, "Completa todos los campos", "", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()
	p, err := Entidades.CrearFuncionario(funcionario, tx)

	if err != nil {

		if err.Error() == "N" {
			MoGeneral.Responder(405, "El rol ya esta asignado", "", ctx)
			return
		}

		tx.Rollback()
		MoGeneral.Responder(http.StatusNotAcceptable, "El funcionario ya se encuentra asignado ", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 18
	movimiento.Extra = funcionario.PersonaId
	movimiento.Extra2 = funcionario.SeccionId
	movimiento.Observacion = "Se asigno el funcionario"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Creada correctamente", p, ctx)
	return
}

func ReemplazarFuncionario(ctx *gin.Context) {
	var funcionario Entidades.FuncionariosHistorial
	err := ctx.BindJSON(&funcionario)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al crear la casilla", []string{err.Error()}, ctx)
		return
	}

	if funcionario.Rol == "" {
		MoGeneral.Responder(http.StatusBadRequest, "Completa todos los campos", "", ctx)
		return
	}
	var f1 Entidades.Funcionarios
	f1.Rol = funcionario.Rol
	f1.PersonaId = funcionario.PersonaId
	f1.CasillaId = funcionario.CasillaId
	f1.SeccionId = funcionario.SeccionId

	tx := Entidades.IniciarTransaccion()
	f, f2, err := Entidades.ReemplazarFuncionario(f1, funcionario.Observacion, tx)

	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusNotAcceptable, "El funcionario ya se encuentra asignado ", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 34
	movimiento.Extra = funcionario.PersonaId
	movimiento.Extra2 = f2.SeccionId
	movimiento.Observacion = "Se reemplazo este funcionario"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Creada correctamente", f, ctx)
	return
}

func EliminarFuncionario(ctx *gin.Context) {
	var funcionario Entidades.Funcionarios

	err := ctx.BindJSON(&funcionario)
	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	if funcionario.PersonaId == "" || funcionario.SeccionId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "Datos incompletos", "Campos incompletos", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	err = Entidades.EliminarFuncionario(funcionario, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al eliminar", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 19
	movimiento.Extra = funcionario.PersonaId
	movimiento.Extra2 = funcionario.SeccionId
	movimiento.Observacion = "Se elimino el funcionario"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", err.Error(), ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah eliminado correctamente", "", ctx)
	return

}

func ObtenerFuncionariosFiltrados(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)

	funcionarios, err := Entidades.FindAllFuncionarios(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", funcionarios, ctx)
	return
}
