package GrupoController

import (
	"github.com/gin-gonic/gin"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Session"
	"fmt"
	"net/http"
	"time"
	"gopkg.in/mgo.v2/bson"
	"strings"
)

func CrearGrupo(ctx *gin.Context) {
	var grupo Entidades.Grupos
	err := ctx.BindJSON(&grupo)

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al crear grupo", err.Error(), ctx)
		return
	}

	grupo.Clave = strings.ToUpper(grupo.Clave)
	grupo.Area = strings.ToUpper(grupo.Area)
	grupo.Zona = strings.ToUpper(grupo.Zona)
	grupo.Id = bson.NewObjectId().Hex()

	tx := Entidades.IniciarTransaccion()

	s, err := Entidades.CrearGrupo(grupo, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusConflict, "Es posible que la clave ya este registrada", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 7
	movimiento.Extra = grupo.Id
	movimiento.Observacion = "Se creo el grupo"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se agrego el grupo correctamente", s, ctx)
	return
}

func ObtenerFiltradosGrupos(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)

	grupos, err := Entidades.FindAllGrupos(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", grupos, ctx)
	return
}

func CancelarGrupo(ctx *gin.Context) {
	id := ctx.Query("id")

	if id == "" {
		MoGeneral.Responder(http.StatusBadRequest, "Grupo sin identificador", "", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()
	s, err := Entidades.CancelarGrupo(id, tx)
	if err != nil {
		fmt.Println("Error: ", err)
		tx.Rollback()
		MoGeneral.Responder(http.StatusConflict, "Problema al eliminar grupo", []string{err.Error()}, ctx)
		return
	}
	tx.Commit()

	MoGeneral.Responder(http.StatusOK, "Eliminado correctamente", s, ctx)
	return
}

func EliminarGrupo(ctx *gin.Context) {
	var grupo Entidades.Grupos
	err := ctx.BindJSON(&grupo)

	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	if grupo.Id == "" {
		MoGeneral.Responder(http.StatusBadRequest, "No se puede eliminar sin identificador", "", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	err = Entidades.EliminarGrupo(grupo, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al eliminar grupo", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 8
	movimiento.Extra = grupo.Id
	movimiento.Observacion = "Se elimino el grupo"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Eliminado correctamente", "", ctx)
	return
}

func ObtenerGrupos(ctx *gin.Context) {
	grupos, err := Entidades.ObtenerGrupos()

	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al obtener grupos", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Resultados obtenidos", grupos, ctx)
	return
}
