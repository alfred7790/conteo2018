package EstructuraController

import (
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Session"
	"net/http"
	"time"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func InsertarEstructura(ctx *gin.Context) {
	var estructura Entidades.Estructuras
	err := ctx.BindJSON(&estructura)

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al crear partido", err.Error(), ctx)
		return
	}
	estructura.Id = bson.NewObjectId().Hex()
	tx := Entidades.IniciarTransaccion()

	s, err := Entidades.RegistrarEstructura(estructura, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusConflict, "Problema al crear partido", []string{err.Error()}, ctx)
		return
	}

	IdUsuario := Session.GetUserID(ctx.Request)
	var movimiento Entidades.Movimientos
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 33
	movimiento.Extra = estructura.Id
	movimiento.Observacion = "Se registro la estructura"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se agrego el partido correctamente", s, ctx)
	return
}

func ObtenerEstructura(ctx *gin.Context) {
	estructuras, err := Entidades.ObtenerEstructuras()

	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al obtener estructuras", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Resultados obtenidos", estructuras, ctx)
	return
}
