package LoginController

import (
	"net/http"
	"github.com/gin-gonic/gin"
	"../../Modulos/General"
	"../../Modelos/Entidades"
	"../../Modulos/Session"
)

func Login(ctx *gin.Context) {

	var loginReq requestLogin
	err := ctx.BindJSON(&loginReq)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Error al leer peticion", []string{err.Error()}, ctx)
		return
	}
	usuario, err := Entidades.LoguearUsuario(loginReq.Username, loginReq.Password)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Error al autenticarse", []string{err.Error()}, ctx)
		return
	}

	token, err := Session.CreateTokenString(usuario.Usuario, usuario.RolId, usuario.Id)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Error al autenticarse", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Ok", token, ctx)
	return
}

func Auth(ctx *gin.Context) {
	valido, err := Session.ValidaToken(ctx.Request)
	if !valido {
		MoGeneral.Responder(401, "Vuelva a iniciar sesión por favor", "", ctx)
		ctx.Abort()
		return
	}
	if err != nil {
		MoGeneral.Responder(401, "Vuelva a iniciar sesión por favor", "", ctx)
		ctx.Abort()
		return
	}
	ctx.Next()
}

func RefreshToken(ctx *gin.Context) {
	token := ctx.Query("token")
	if token != "" {
		nuevoToken, err := Session.RefreshToken(token)
		if err != nil {
			MoGeneral.Responder(400, "No se pudo renovar token", []string{err.Error()}, ctx)
			return
		}
		MoGeneral.Responder(200, "ok", nuevoToken, ctx)
		return
	} else {
		MoGeneral.Responder(400, "Token invalido", []string{"No ha especificado un token válido"}, ctx)
		return
	}

}