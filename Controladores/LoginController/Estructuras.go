package LoginController

/**
Autor: Miguel Ángel Bautista Cruz
Fecha: 04/03/2018
Descripción: Estructura para poder pedir una autenticación
*/
type requestLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
