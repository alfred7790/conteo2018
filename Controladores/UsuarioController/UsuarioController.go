package UsuarioController

import (
	"github.com/gin-gonic/gin"
	"fmt"
	"net/http"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Cifrado"
)

func CrearUsuario(ctx *gin.Context) {
	var usuario Entidades.Usuarios
	err := ctx.BindJSON(&usuario)
	if err != nil {
		fmt.Println("Error al pasar la estructura")
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al convertir la estructura", []string{err.Error()}, nil)
		return
	}

	usuario.Password = Cifrado.Cryp(usuario.Password)

	tx := Entidades.IniciarTransaccion()
	p, err := Entidades.CrearUsuario(usuario, tx)
	if err != nil {
		fmt.Println("Error al crear usuario")
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al crear usuario", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Creada correctamente", p, ctx)
	return
}
