package PromovidoController

import (
	"github.com/gin-gonic/gin"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"../../Modulos/Session"
	"net/http"
	"time"
)

func RegistroPromovidos(ctx *gin.Context) {
	var votante Entidades.Votantes

	err := ctx.BindJSON(&votante)

	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al recibir datos", err.Error(), ctx)
		return
	}
	votante.VotoFinal = false
	tx := Entidades.IniciarTransaccion()

	c, err := Entidades.RegistrarPromovido(votante, tx)

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Ya fue registrado con este promotor", err.Error(), ctx)
		tx.Rollback()
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 28
	movimiento.Extra = c.PersonapromotorId
	movimiento.Extra2 = c.PersonaseccionId
	movimiento.Observacion = "Registro de promovido"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", []string{err.Error()}, ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah registrado correctamente", "", ctx)
	return
}

func EliminarPromovido(ctx *gin.Context) {
	var promovido Entidades.Votantes

	err := ctx.BindJSON(&promovido)
	if err != nil {
		MoGeneral.Responder(http.StatusConflict, "Problema al convertir la estructura", []string{err.Error()}, ctx)
		return
	}

	if promovido.PersonavotanteId == "" || promovido.PersonapromotorId == "" {
		MoGeneral.Responder(http.StatusBadRequest, "No se puede eliminar, si no es promovido", "No existe el id", ctx)
		return
	}

	tx := Entidades.IniciarTransaccion()

	err = Entidades.EliminarPromovido(promovido, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al eliminar", []string{err.Error()}, ctx)
		return
	}

	var movimiento Entidades.Movimientos
	IdUsuario := Session.GetUserID(ctx.Request)
	movimiento.UsuarioId = IdUsuario
	movimiento.Fecha = time.Now()
	movimiento.ActividadId = 29
	movimiento.Extra = promovido.PersonavotanteId
	movimiento.Extra2 = promovido.PersonapromotorId
	movimiento.Observacion = "Se elimino el promovido"

	err = Entidades.RegistrarMovimientos(movimiento, tx)
	if err != nil {
		tx.Rollback()
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al registrar movimiento", err.Error(), ctx)
		return
	}

	tx.Commit()
	MoGeneral.Responder(http.StatusOK, "Se ah eliminado correctamente", "", ctx)
	return

}

func FindListaPromovidos(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)
	personas, err := Entidades.FindListaPromovidos(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}
	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", personas, ctx)
	return
}

