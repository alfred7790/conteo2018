package Reportes

import (
	"github.com/gin-gonic/gin"
	"../../Modelos/Entidades"
	"net/http"
	"../../Modulos/General"
	"../../Modulos/Xls"
	"fmt"
	"encoding/base64"
	"bytes"
	"io"
	"encoding/json"
)

func CoordxDistrito(ctx *gin.Context) {
	coor, err := Entidades.ObtenerCoordxDistrito()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	var coor2 []Entidades.CoordPorDistrito2
	b, err := json.Marshal(coor)
	err = json.Unmarshal(b, &coor2)

	excel, err := Xls.CoorxDistritoExcel(coor2)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)

}

func CoordxDistritoSeccion(ctx *gin.Context) {
	coor, err := Entidades.ObtenerCoordxSeccionDistrito()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	var coor2 []Entidades.CoordPorSeccionDistrito2
	b, err := json.Marshal(coor)
	err = json.Unmarshal(b, &coor2)

	excel, err := Xls.CoorxSeccionDistritoExcel(coor2)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}

func TotalxDistrito(ctx *gin.Context) {
	coor, err := Entidades.ObtenerReporteTotalDistrito()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	excel, err := Xls.TotalDistrito(*coor)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}

func TotalxDistritoSeccion(ctx *gin.Context) {
	coor, err := Entidades.ObtenerReporteTotalDistritoSeccion()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	excel, err := Xls.TotalDistritoSeccion(*coor)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}

func PromovidosRepetidos(ctx *gin.Context) {
	coor, err := Entidades.ObtenerReportePromovidosRepetidos()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	excel, err := Xls.ReportePromovidosRepetidos(*coor)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}

func ReporteVotosTotalPartido(ctx *gin.Context) {
	concentrado, err := Entidades.ObtenerVotosTotalesPartido()

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", err.Error(), ctx)
		return
	}

	excel, err := Xls.TotalVotosPartido(*concentrado)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}

func ReportePromovidosPorEstructura(ctx *gin.Context) {
	promovidos, err := Entidades.ObtenerPromovidosPorEstructura()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	excel, err := Xls.ReportePromovidosPorEstructura(*promovidos)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}

func ReportePromovidosPorEstructuraSeccion(ctx *gin.Context) {
	promovidos, err := Entidades.ObtenerPromovidosPorEstructuraSeccion()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	excel, err := Xls.ReportePromovidosPorEstructuraSeccion(*promovidos)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}

func ReporteCasillasIncidencias(ctx *gin.Context) {
	casillas, err := Entidades.ObtenerCasillasIncidencias()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	excel, err := Xls.ReporteCasillasIncidencias(*casillas)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}

func ReporteMpp(ctx *gin.Context) {
	casillas, err := Entidades.ObtenerCoordMpp()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	excel, err := Xls.ReporteMpp(*casillas)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}

func ReporteCasillaDireccionHistorial(ctx *gin.Context) {
	casillas, err := Entidades.ObtenerCasillaDireccionHistorial()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	excel, err := Xls.ReporteCasillaDireccionHistorial(*casillas)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}

func ReporteFuncionarioCasillaHistorial(ctx *gin.Context) {
	funcionario, err := Entidades.ObtenerFuncionarioCasillaHistorial()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	excel, err := Xls.ReporteFuncionarioCasillaHistorial(*funcionario)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}

func PromovidosRepetidosEstructura(ctx *gin.Context) {
	promovidos, err := Entidades.ObtenerPromovidosRepetidosEstructura()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	excel, err := Xls.ReportePromovidosRepetidosEstructuras(*promovidos)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}

func CasillasSinRegistrarVotos(ctx *gin.Context) {
	casillas, err := Entidades.ObtenerCasillasSinRegistrarVotos()

	if err != nil {
		fmt.Println(err)
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", "", ctx)
		return
	}

	excel, err := Xls.ReporteCasillaSinRegistrarVotos(*casillas)
	if err != nil {
		MoGeneral.Responder(http.StatusInternalServerError, "Problema al generar reporte", err.Error(), ctx)
	}

	archivo, _ := base64.StdEncoding.DecodeString(excel)
	ctx.Writer.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	buffer := bytes.NewBuffer([]byte(archivo))
	io.Copy(ctx.Writer, buffer)
}
