package Reportes

import (
	"github.com/gin-gonic/gin"
	"../../Modelos/Entidades"
	"../../Modulos/General"
	"net/http"
)

func ConcentradoTotal(ctx *gin.Context) {
	concentrado, err := Entidades.ObtenerConcentradoTotal()

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", err.Error(), ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Datos obtenidos correctamente", concentrado, ctx)
	return
}

func VotosTotalPartido(ctx *gin.Context) {
	concentrado, err := Entidades.ObtenerVotosTotalesPartido()

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", err.Error(), ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Datos obtenidos correctamente", concentrado, ctx)
	return
}

func VotosTotalCandidato(ctx *gin.Context) {
	concentrado, err := Entidades.ObtenerVotosTotalesCandidato()

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", err.Error(), ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Datos obtenidos correctamente", concentrado, ctx)
	return
}

func ObtenerPromovidosPorEstructura(ctx *gin.Context) {
	concentrado, err := Entidades.ObtenerPromovidosPorEstructura()

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", err.Error(), ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Datos obtenidos correctamente", concentrado, ctx)
	return
}

func ObtenerTotalPromovidos(ctx *gin.Context) {
	concentrado, err := Entidades.ObtenerTotalPromovidos()

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", err.Error(), ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Datos obtenidos correctamente", concentrado, ctx)
	return
}

func ObtenerEstadoCasillas(ctx *gin.Context) {
	concentrado, err := Entidades.ObtenerEstadoCasillas()

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", err.Error(), ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Datos obtenidos correctamente", concentrado, ctx)
	return
}

func ObtenerEstadoCasillasCerradas(ctx *gin.Context) {
	concentrado, err := Entidades.ObtenerEstadoCasillasCerradas()

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", err.Error(), ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Datos obtenidos correctamente", concentrado, ctx)
	return
}


func ObtenerVotosCasilla(ctx *gin.Context) {
	concentrado, err := Entidades.ObtenerVotosCasillas()

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", err.Error(), ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Datos obtenidos correctamente", concentrado, ctx)
	return
}

func FindVotosPartidoSeccion(ctx *gin.Context) {
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)

	incidencias, err := Entidades.FindVotosPartidoSeccion(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", incidencias, ctx)
	return
}

func BuscarSeccionPorVotosPartido(ctx *gin.Context){
	idSeccion := ctx.Query("seccion")
	seccion, err := Entidades.BuscarSeccion2(idSeccion)

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "La seccion no se encuentra, vuelva a intentarlo", err.Error(), ctx)
		return
	}


	secciones, err := Entidades.ObtenerVotosPartidoSeccion(seccion.Id)

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", err.Error(), ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", secciones, ctx)
	return
}

func ObtenerFlujoVotosEstimados(ctx *gin.Context) {
	concentrado, err := Entidades.ObtenerFlujoVotosEstimados()

	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", err.Error(), ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Datos obtenidos correctamente", concentrado, ctx)
	return
}

func ObtenerColonias(ctx *gin.Context) {
	colonias, err := Entidades.ObtenerColonias()
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Problema al consultar", err.Error(), ctx)
		return
	}

	MoGeneral.Responder(http.StatusOK, "Datos obtenidos correctamente", colonias, ctx)
	return
}

func ObtenerColoniasPartidosVotos(ctx *gin.Context){
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)
	personas, err := Entidades.ObtenerColoniasVotos(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}
	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", personas, ctx)
	return
}

func FindColonias(ctx *gin.Context){
	filtros, err := Entidades.GetFiltros(ctx)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al leer los filtros", []string{err.Error()}, ctx)
		return
	}

	limit, page := Entidades.GetLimitPage(ctx)
	personas, err := Entidades.FindColoniasFiltradas(limit, page, filtros)
	if err != nil {
		MoGeneral.Responder(http.StatusBadRequest, "Existió un error al consultar", []string{err.Error()}, ctx)
		return
	}
	MoGeneral.Responder(http.StatusOK, "Completado exitosamente", personas, ctx)
	return
}