﻿select
  seccion_id,
  tipo,
  count(tipo) as total
from lista_coordinadores
group by seccion_id, tipo

select count(id) as total
from lista_coordinadores

select sum(total)
from (select
        seccion_id,
        tipo,
        count(tipo) as total
      from lista_coordinadores
      group by seccion_id, tipo) as a

create view Lista_representantes as (
  select
    p.id,
    p.nombre,
    p.app,
    p.apm,
    t.descripcion as tipo
  from personas p inner join persona_representante r on p.id = r.persona_id
    inner join tipo_coordinador t on t.id = r.tipo_representante_id)


create view coord_por_distrito as
  (select
     promotor.distrito,
     promotor.tipo      as cargo,
     promotor.total     as total_promotor,
     coordmanzana.tipo  as cargoo,
     coordmanzana.total as total_coordmanzana
   from
     (select
        distrito,
        tipo,
        total
      from
        (select
           distrito,
           tipo,
           sum(total) as total
         from (select
                 ls.seccion_id,
                 ls.tipo,
                 count(ls.tipo) as total,
                 s.distrito     as distrito
               from lista_coordinadores ls inner join secciones s on ls.seccion_id = s.id
               group by ls.seccion_id, ls.tipo, distrito) sd
         GROUP BY distrito, tipo
         order by distrito) as d
      where tipo = 'PROMOTOR') as promotor
     left join
     (select
        distrito,
        tipo,
        total
      from
        (select
           distrito,
           tipo,
           sum(total) as total
         from (select
                 ls.seccion_id,
                 ls.tipo,
                 count(ls.tipo) as total,
                 s.distrito     as distrito
               from lista_coordinadores ls inner join secciones s on ls.seccion_id = s.id
               group by ls.seccion_id, ls.tipo, distrito) sd
         GROUP BY distrito, tipo
         order by distrito) as d
      where tipo = 'COORDINADOR DE MANZANA') as coordmanzana on promotor.distrito = coordmanzana.distrito)

--total de coordinadores por seccion y distrito
create view coord_por_seccion_distrito as
  (select
     prom.distrito,
     prom.seccion_id,
     prom.tipo    as tipo,
     prom.total   as total,
     coordm.tipo  as tipoo,
     coordm.total as totaal
   from
     (select
        ls.seccion_id,
        s.distrito     as distrito,
        ls.tipo,
        count(ls.tipo) as total
      from lista_coordinadores ls inner join secciones s on ls.seccion_id = s.id
      where tipo = 'COORDINADOR DE MANZANA'
      group by ls.seccion_id, ls.tipo, distrito
      order by seccion_id, distrito) as prom
     RIGHT JOIN
     (select
        ls.seccion_id,
        s.distrito     as distrito,
        ls.tipo,
        count(ls.tipo) as total
      from lista_coordinadores ls inner join secciones s on ls.seccion_id = s.id
      where tipo = 'PROMOTOR'
      group by ls.seccion_id, ls.tipo, distrito
      order by seccion_id, distrito) as coordm
       on prom.seccion_id = coordm.seccion_id and prom.distrito = coordm.distrito
   order by distrito, seccion_id)

create view Lista_coordinadores as (
  select
    p.id             as persona_id,
    p.nombre,
    p.app,
    p.apm,
    p.clave_elec     as clave_elector,
    p.seccion_id,
    t.descripcion    as tipo,
    pt.id            as partido_id,
    pt.nombre        as partido,
    pt.abreviaciones as abreviacion
  from personas p inner join persona_coordinador c on p.id = c.persona_id
    inner join tipo_coordinador t on t.id = c.tipo_coordinador_id
    left join partidos pt on pt.id = c.partido_id);

create view Lista_coordinadores_seccion as
  (
    select
      p.id             as personaseccion_id,
      p.nombre         as personaseccion_nombre,
      p.app            as personaseccion_app,
      p.apm            as personaseccion_apm,
      g.id             as grupo_id,
      g.clave          as grupo_clave,
      cs.seccion_id    as seccion_id,
      p2.id            as partido_id,
      p2.nombre        as partido_nombre,
      p2.abreviaciones as partido_abreviacion
    from coordinador_secciones cs inner join personas p on cs.personaseccion_id = p.id
      inner join grupos g on cs.grupo_id = g.id
      inner join partidos p2 on g.partido_id = p2.id
  );

create view Lista_coordinadores_manzana as
  (
    SELECT
      dd.personamanzana_id,
      dd.personamanzana_nombre,
      dd.personamanzana_app,
      dd.personamanzana_apm,
      dd.personamanzana_seccion_id as seccion_id,
      dd.grupo_id,
      dd.partido_id,
      dd.partido                   as partido_nombre,
      dd.abreviacion               as partido_abreviacion,
      pp.id                        as personaseccion_id,
      pp.nombre                    as personaseccion_nombre,
      pp.app                       as personaseccion_app,
      pp.apm                       as personaseccion_apm
    FROM
      (
        SELECT
          P.ID                 AS personamanzana_id,
          P.nombre             as personamanzana_nombre,
          P.app                as personamanzana_app,
          P.apm                as personamanzana_apm,
          cm.seccion_id        as personamanzana_seccion_id,
          g.id                 as grupo_id,
          p2.ID                AS partido_id,
          p2.nombre            AS partido,
          p2.abreviaciones     AS abreviacion,
          cm.personaseccion_id AS personaseccion_id
        FROM
          coordinador_manzanas cm
          INNER JOIN personas P ON cm.personamanzana_id = P.
        ID
          INNER JOIN grupos G ON cm.grupo_id = G.
        ID
          INNER JOIN partidos p2 ON G.partido_id = p2.ID
      ) AS dd
      INNER JOIN personas pp ON dd.personaseccion_id = pp.ID
  );

create view Lista_promotores as
  (select
     dd.personapromotor_id,
     dd.personapromotor_nombre,
     dd.personapromotor_app,
     dd.personapromotor_apm,
     dd.partido_id,
     dd.partido_nombre,
     dd.partido_abreviacion,
     dd.grupo_id,
     dd.seccion_id,
     dd.personaseccion_id,
     dd.personamanzana_id,
     pp.nombre as personamanzana_nombre,
     pp.app    as personamanzana_app,
     pp.apm    as personamanzana_apm
   from
     (select
        p.personapromotor_id,
        p2.nombre        as personapromotor_nombre,
        p2.app           as personapromotor_app,
        p2.apm           as personapromotor_apm,
        p.grupo_id,
        p.seccion_id,
        p.personaseccion_id,
        g.partido_id     as partido_id,
        p3.nombre        as partido_nombre,
        p3.abreviaciones as partido_abreviacion,
        p.personamanzana_id
      from promotores p inner join personas p2 on p.personapromotor_id = p2.id
        inner join coordinador_manzanas cm on p.personamanzana_id = cm.personamanzana_id
        inner join grupos g on g.id = p.grupo_id
        inner join partidos p3 on g.partido_id = p3.id) as dd inner join personas as pp
       on dd.personamanzana_id = pp.id);

create view Lista_promovidos as (

)

select
  v.personavotante_id,
  p.nombre         as personavotante_nombre,
  p.app            as personavotante_app,
  p.apm            as personavotante_apm,
  p2.id            as partido_id,
  p2.nombre        as partido_nombre,
  p2.abreviaciones as partido_abreviacion,
  v.personapromotor_id
from votantes v inner join personas p on v.personavotante_id = p.id
  inner join grupos g on v.grupo_id = g.id
  inner join partidos p2 on g.partido_id = p2.id