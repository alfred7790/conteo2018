package Session

import (
	"errors"
	"net/http"
	"strings"
	"time"
	"github.com/dgrijalva/jwt-go"
)

//UsuarioLogeado Estrcutura de control de usuarios logueados.
type UsuarioLogeado struct {
	Username string `json:"username"`
	Profile  int    `json:"profile"`
	Id       int    `json:"id"`
	jwt.StandardClaims
}

var key = []byte("secret")

//CreateTokenString función que crea el token de tipo string a partir de los datos de un usuario.
func CreateTokenString(username string, profile, id int) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := UsuarioLogeado{}
	claims.ExpiresAt = time.Now().Add(time.Hour * time.Duration(12)).Unix()

	claims.IssuedAt = time.Now().Unix()
	claims.Username = username
	claims.Profile = profile
	claims.Id = id
	token.Claims = claims
	tokenString, err := token.SignedString(key)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func decodeToken(r *http.Request) (*UsuarioLogeado, error) {
	header := r.Header.Get("Authorization")
	if header == "" {
		return nil, errors.New("No hay cabecera de autenticacion")
	}
	token := strings.Split(header, " ")[1]
	var usuario UsuarioLogeado
	_, err := jwt.ParseWithClaims(token, &usuario, func(token *jwt.Token) (interface{}, error) { return key, nil })
	if err != nil {
		return nil, err
	}
	return &usuario, nil
}

//ValidaToken un token para su vigencia
func ValidaToken(r *http.Request) (bool, error) {
	header := r.Header.Get("Authorization")
	if header == "" {
		return false, errors.New("No hay cabecera  Authorization")
	}
	tokens := strings.Split(header, " ")[1]
	var usuario UsuarioLogeado
	token, err := jwt.ParseWithClaims(tokens, &usuario, func(token *jwt.Token) (interface{}, error) { return key, nil })
	if err != nil {
		return false, err
	}
	if usuario.ExpiresAt < time.Now().Unix() {
		return false, errors.New("Vuelva a iniciar sesion")
	}
	return token.Valid, nil
}

//GetUserName regresa el nombre del usuario logueado
func GetUserName(r *http.Request) (userName, Nivel string, ID int) {
	usuario, err := decodeToken(r)
	if err != nil {
		userName = ""
		Nivel = ""
		ID = -1
	} else {
		userName = usuario.Username
		Nivel = ""
		ID = usuario.Id
	}
	return
}

//GetProfile regresa el tipo de usuario del usuario logueado
func GetProfile(request *http.Request) int {
	usuario, err := decodeToken(request)
	if err != nil {
		return -1
	}
	return usuario.Profile
}

//GetUserID regresa el ID del usuario logueado
func GetUserID(request *http.Request) int {
	usuario, err := decodeToken(request)
	if err != nil {
		return -1
	}
	return usuario.Id
}

func RefreshToken(token string) (string, error) {
	var usuario UsuarioLogeado
	_, err := jwt.ParseWithClaims(token, &usuario,
		func(token *jwt.Token) (interface{}, error) { return key, nil })
	if err != nil {
		return "", err
	}

	nuevoToken, err := CreateTokenString(usuario.Username, usuario.Profile, usuario.Id)
	if err != nil {
		return "", err
	}
	return nuevoToken, nil
}