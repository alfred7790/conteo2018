package MoVar

import (
	"github.com/robfig/config"
)

const (
	//TipoAlmacen tipo a manejar en elastic
	TipoAlmacen = "Almacen"
)

//DataCfg estructura de datos del entorno
type DataCfg struct {
	BaseURL    string
	Servidor   string
	Puerto     string
	Usuario    string
	Pass       string
	Protocolo  string
	NombreBase string
}

var Soporte = []string{"korereports@outlook.com"}

const (
	ModuloLocal = "Modulo_Base"
	PerfilAdmin = "1"
	//Frecuencia contiene la frecuencia o el tiempo que tardará cada ciclo de monitoreo
	Frecuencia = uint64(30)

	//TipoFrecuencia contiene el tipo de tiempo en que se repetirá cada ciclo
	//0-minutos , 1-segundos, 2-horas, 3-dias, 4-semanas, 5-lunes,
	//6-martes, 7-miercoles, 8-jueves, 9-viernes, 10-sabado, 11-domingo, default minutos;
	TipoFrecuencia = 1

	//ArchivosCollection nombre de la coleccion para guardar archivos en  Mongo
	ArchivosCollection = "Imagenes"

	//##Archivo de configuracion
	FileConfigName = "Acfg.cfg"

	//################ SECCIONES CFG  ######################################

	//SecDefault nombre de la seccion default del servidor en CFG
	SecDefault = "DEFAULT"
	//SecMongo nombre de la seccion de mongo en CFG
	SecMongo = "CONFIG_DB_MONGO"
	//SecElastic nombre de la seccion de firebird en cfg
	SecFirebird = "CONFIG_DB_FIREBIRD"
	//Seccion para la configuracion de MySql
	SecMySql = "CONFIG_DB_MYSQL"

	SecWebServiceVisorus = "CONFIG_DE_VISORUS"
	SecPsql = "CONFIG_DB_POSTGRES"
	SecWsMonitoreo = "CONFIG_WS_MONITOREO_DE_ERRORES"

	//ColeccionAlmacen nombre de la coleccion de MAEMOVCFD en mongo
	ColeccionPrueba   = "Prueba"
	ColeccionEnvio    = "Envio"
	ColeccionRuta     = "Ruta"
	ColeccionUser     = "User"
	ColeccionPost     = "Post"
	ColeccionFolio    = "Folio"
	ColeccionVehiculo = "Vehiculo"
	ColeccionChofer   = "Chofer"
)

//CargaSeccionCFG rellena los datos de la seccion a utilizar
func CargaSeccionCFG(seccion string) DataCfg {
	var d DataCfg
	var FileConfig, err = config.ReadDefault(FileConfigName)
	if err == nil {
		if FileConfig.HasOption(seccion, "baseurl") {
			d.BaseURL, _ = FileConfig.String(seccion, "baseurl")
		}
		if FileConfig.HasOption(seccion, "servidor") {
			d.Servidor, _ = FileConfig.String(seccion, "servidor")
		}
		if FileConfig.HasOption(seccion, "puerto") {
			d.Puerto, _ = FileConfig.String(seccion, "puerto")
		}
		if FileConfig.HasOption(seccion, "usuario") {
			d.Usuario, _ = FileConfig.String(seccion, "usuario")
		}
		if FileConfig.HasOption(seccion, "pass") {
			d.Pass, _ = FileConfig.String(seccion, "pass")
		}
		if FileConfig.HasOption(seccion, "protocolo") {
			d.Protocolo, _ = FileConfig.String(seccion, "protocolo")
		}
		if FileConfig.HasOption(seccion, "base") {
			d.NombreBase, _ = FileConfig.String(seccion, "base")
		}
	}
	return d
}
