package Xls

import (
	"../../Modelos/Entidades"
	"github.com/tealeg/xlsx"
	"fmt"
	"os"
	"io/ioutil"
	"encoding/base64"
	"time"
	"strconv"
)

func GenerarXml() {
	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell
	var err error

	type Prueba struct {
		Nombre   string
		Apellido string
		Edad     int
		Estado   bool
	}

	e := Prueba{
		"Lalo",
		"Venegas",
		23,
		true,
	}

	file = xlsx.NewFile()
	sheet, err = file.AddSheet("Hoja 1 Prueba")
	if err != nil {
		fmt.Printf(err.Error())
	}
	row = sheet.AddRow()
	cell = row.AddCell()
	cell.Value = "CELDA DE PRUEBA!"

	row.WriteStruct(&e, -1)
	err = file.Save("Prueba.xlsx")
	if err != nil {
		fmt.Printf(err.Error())
	}
}

func CoorxDistritoExcel(coordinadores []Entidades.CoordPorDistrito2) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var totalPromotor, totalManzana int

	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Hoja 1")
	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "DISTRITO"
	cell = row.AddCell()
	cell.Value = "PROMOTOR"
	cell = row.AddCell()
	cell.Value = "COORDINADOR DE MANZANA"
	cell = row.AddCell()

	for i := range coordinadores {
		row = hoja.AddRow()
		coordinadores[i].Distrito = coordinadores[i].Distrito[0:2]
		row.WriteStruct(&coordinadores[i], -1)
		totalPromotor += coordinadores[i].TotalPromotor
		totalManzana += coordinadores[i].TotalCoordmanzana
	}

	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "TOTAL"
	cell = row.AddCell()
	cell.SetInt(totalPromotor)
	cell = row.AddCell()
	cell.SetInt(totalManzana)

	err = file.Save("CoordXDistrito.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("CoordXDistrito.xlsx")
	if err != nil {
		return "", err
	}

	os.Remove("CoordXDistrito.xlsx")

	return base64.StdEncoding.EncodeToString(data), nil

}

func CoorxSeccionDistritoExcel(coordinadores []Entidades.CoordPorSeccionDistrito2) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var totalPromotor, totalManzana int
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Hoja 1")
	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "DISTRITO"
	cell = row.AddCell()
	cell.Value = "SECCION"
	cell = row.AddCell()
	cell.Value = "COORDINADOR DE MANZANA"
	cell = row.AddCell()
	cell.Value = "PROMOTOR"

	for i := range coordinadores {
		row = hoja.AddRow()
		//	coordinadores[i].Distrito = coordinadores[i].Distrito[0:2]
		row.WriteStruct(&coordinadores[i], -1)
		totalPromotor += coordinadores[i].Totaal
		totalManzana += coordinadores[i].Total
	}

	row = hoja.AddRow()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "TOTAL"
	cell = row.AddCell()
	cell.SetInt(totalManzana)
	cell = row.AddCell()
	cell.SetInt(totalPromotor)

	err = file.Save("CoordXSeccionDistrito.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("CoordXSeccionDistrito.xlsx")
	if err != nil {
		return "", err
	}
	os.Remove("CoordXSeccionDistrito.xlsx")

	return base64.StdEncoding.EncodeToString(data), nil

}

func TotalDistrito(coordinadores []Entidades.ReporteTotalDistrito) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var cols []*xlsx.Col
	var totalPromotor, totalManzana, totalSeccion, totalZona, totalPromovido int
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Total x Distrito")

	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "FECHA"
	cell = row.AddCell()
	t := time.Now().String()
	cell.Value = t[0:10]
	cell = row.AddCell()
	cell.Value = "HORA"
	cell = row.AddCell()
	hora := time.Now()
	cell.Value = strconv.Itoa(hora.Hour()) + ":" + strconv.Itoa(hora.Minute())

	hoja.AddRow()
	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "DISTRITO"
	cell = row.AddCell()
	cell.Value = "TIPO"
	cell = row.AddCell()
	cell.Value = "PARTIDO"
	cell = row.AddCell()
	cell.Value = "TOTAL"

	for i := range coordinadores {
		row = hoja.AddRow()
		coordinadores[i].Distrito = coordinadores[i].Distrito[0:2]
		row.WriteStruct(&coordinadores[i], -1)
		if coordinadores[i].Tipo == "COORDINADOR DE MANZANA" {
			totalManzana += coordinadores[i].Total
		}
		if coordinadores[i].Tipo == "PROMOTOR" {
			totalPromotor += coordinadores[i].Total
		}
		if coordinadores[i].Tipo == "COORDINADOR DE SECCION" {
			totalSeccion += coordinadores[i].Total
		}
		if coordinadores[i].Tipo == "COORDINADOR DE ZONA" {
			totalZona += coordinadores[i].Total
		}
		if coordinadores[i].Tipo == "PROMOVIDOS" {
			totalPromovido += coordinadores[i].Total
		}
	}

	hoja.AddRow()
	row = hoja.AddRow()
	row.AddCell()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "CONCENTRADO"

	row = hoja.AddRow()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "COORDINADOR DE ZONA"
	cell = row.AddCell()
	cell.SetInt(totalZona)

	row = hoja.AddRow()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "COORDINADOR DE SECCION"
	cell = row.AddCell()
	cell.SetInt(totalSeccion)

	row = hoja.AddRow()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "COORDINADOR DE MANZANA"
	cell = row.AddCell()
	cell.SetInt(totalManzana)

	row = hoja.AddRow()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "PROMOTORES"
	cell = row.AddCell()
	cell.SetInt(totalPromotor)

	row = hoja.AddRow()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "PROMOVIDOS"
	cell = row.AddCell()
	cell.SetInt(totalPromovido)

	cols = hoja.Cols

	cols[1].Width = 26.4
	cols[2].Width = 13

	err = file.Save("ReporteTotalxDistrito.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("ReporteTotalxDistrito.xlsx")
	if err != nil {
		return "", err
	}
	os.Remove("ReporteTotalxDistrito.xlsx")
	return base64.StdEncoding.EncodeToString(data), nil

}

func TotalDistritoSeccion(coordinadores []Entidades.ReporteTotalDistritoSeccion) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var cols []*xlsx.Col
	var totalPromotor, totalManzana, totalSeccion, totalZona, totalPromovido int
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Total x Distrito")

	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "FECHA"
	cell = row.AddCell()
	t := time.Now().String()
	cell.Value = t[0:10]
	cell = row.AddCell()
	cell.Value = "HORA"
	cell = row.AddCell()
	hora := time.Now()
	cell.Value = strconv.Itoa(hora.Hour()) + ":" + strconv.Itoa(hora.Minute())

	hoja.AddRow()
	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "SECCION"
	cell = row.AddCell()
	cell.Value = "DISTRITO"
	cell = row.AddCell()
	cell.Value = "TIPO"
	cell = row.AddCell()
	cell.Value = "PARTIDO"
	cell = row.AddCell()
	cell.Value = "TOTAL"

	for i := range coordinadores {
		row = hoja.AddRow()
		coordinadores[i].Distrito = coordinadores[i].Distrito[0:2]
		row.WriteStruct(&coordinadores[i], -1)
		if coordinadores[i].Tipo == "COORDINADOR DE MANZANA" {
			totalManzana += coordinadores[i].Total
		}
		if coordinadores[i].Tipo == "PROMOTOR" {
			totalPromotor += coordinadores[i].Total
		}
		if coordinadores[i].Tipo == "COORDINADOR DE SECCION" {
			totalSeccion += coordinadores[i].Total
		}
		if coordinadores[i].Tipo == "COORDINADOR DE ZONA" {
			totalZona += coordinadores[i].Total
		}
		if coordinadores[i].Tipo == "PROMOVIDOS" {
			totalPromovido += coordinadores[i].Total
		}
	}

	hoja.AddRow()
	row = hoja.AddRow()
	row.AddCell()
	row.AddCell()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "CONCENTRADO"

	row = hoja.AddRow()
	row.AddCell()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "COORDINADOR DE ZONA"
	cell = row.AddCell()
	cell.SetInt(totalZona)

	row = hoja.AddRow()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "COORDINADOR DE SECCION"
	cell = row.AddCell()
	cell.SetInt(totalSeccion)

	row = hoja.AddRow()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "COORDINADOR DE MANZANA"
	cell = row.AddCell()
	cell.SetInt(totalManzana)

	row = hoja.AddRow()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "PROMOTORES"
	cell = row.AddCell()
	cell.SetInt(totalPromotor)

	row = hoja.AddRow()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "PROMOVIDOS"
	cell = row.AddCell()
	cell.SetInt(totalPromovido)

	cols = hoja.Cols

	cols[1].Width = 10
	cols[2].Width = 26.4
	cols[3].Width = 10
	err = file.Save("ReporteTotalxDistrito.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("ReporteTotalxDistrito.xlsx")
	if err != nil {
		return "", err
	}
	os.Remove("ReporteTotalxDistrito.xlsx")
	return base64.StdEncoding.EncodeToString(data), nil

}

func ReportePromovidosRepetidos(promovidos []Entidades.ListaPromovidosRepetidos) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var cols []*xlsx.Col
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Promovidos Repetidos")

	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	row.AddCell()
	cell := row.AddCell()
	cell.Value = "FECHA"
	cell = row.AddCell()
	t := time.Now().String()
	cell.Value = t[0:10]
	cell = row.AddCell()
	cell.Value = "HORA"
	cell = row.AddCell()
	hora := time.Now()
	cell.Value = strconv.Itoa(hora.Hour()) + ":" + strconv.Itoa(hora.Minute())

	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "PROMOVIDO"
	row.AddCell()
	row.AddCell()
	row.AddCell()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "PROMOTOR"

	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "NOMBRE"
	cell = row.AddCell()
	cell.Value = "APELLIDO PATERNO"
	cell = row.AddCell()
	cell.Value = "APELLIDO MATERNO"
	cell = row.AddCell()
	cell.Value = "SECCION"
	cell = row.AddCell()
	cell.Value = "PARTIDO"
	cell = row.AddCell()
	cell.Value = "NOMBRE"
	cell = row.AddCell()
	cell.Value = "APELLIDO PATERNO"
	cell = row.AddCell()
	cell.Value = "APELLIDO MATERNO"

	for i := range promovidos {
		row = hoja.AddRow()
		row.WriteStruct(&promovidos[i], -1)
	}

	cols = hoja.Cols

	cols[0].Width = 26.9
	cols[1].Width = 17
	cols[2].Width = 17
	cols[3].Width = 8.9
	cols[4].Width = 8.9
	cols[5].Width = 26.9
	cols[6].Width = 17
	cols[7].Width = 17

	err = file.Save("ReportePromovidosRepetidos.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("ReportePromovidosRepetidos.xlsx")
	if err != nil {
		return "", err
	}
	os.Remove("ReportePromovidosRepetidos.xlsx")
	return base64.StdEncoding.EncodeToString(data), nil
}

func TotalVotosPartido(votos []Entidades.VotosPorPartido) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var cols []*xlsx.Col
	var totalVotos int
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Votos")

	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "FECHA"
	cell = row.AddCell()
	t := time.Now().String()
	cell.Value = t[0:10]
	cell = row.AddCell()
	cell.Value = "HORA"
	cell = row.AddCell()
	hora := time.Now()
	cell.Value = strconv.Itoa(hora.Hour()) + ":" + strconv.Itoa(hora.Minute())

	hoja.AddRow()
	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "PARTIDO"
	cell = row.AddCell()
	cell.Value = "TOTAL"

	for i := range votos {
		row = hoja.AddRow()
		row.WriteStruct(&votos[i], -1)
		totalVotos = + votos[i].Total
	}

	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "TOTAL"
	cell = row.AddCell()
	cell.SetInt(totalVotos)

	cols = hoja.Cols

	cols[0].Width = 15.0
	cols[1].Width = 26.4

	err = file.Save("ReporteVotos.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("ReporteVotos.xlsx")
	if err != nil {
		return "", err
	}
	os.Remove("ReporteVotos.xlsx")
	return base64.StdEncoding.EncodeToString(data), nil

}

func ReportePromovidosPorEstructura(promovidos []Entidades.PromovidosPorEstructura) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var cols []*xlsx.Col
	var totalPromovidos int
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Promovidos")

	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "FECHA"
	cell = row.AddCell()
	t := time.Now().String()
	cell.Value = t[0:10]
	cell = row.AddCell()
	cell.Value = "HORA"
	cell = row.AddCell()
	hora := time.Now()
	cell.Value = strconv.Itoa(hora.Hour()) + ":" + strconv.Itoa(hora.Minute())

	hoja.AddRow()
	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "ESTRUCTURA"
	cell = row.AddCell()
	cell.Value = "PROMOVIDOS"

	for i := range promovidos {
		row = hoja.AddRow()
		row.WriteStruct(&promovidos[i], -1)
		totalPromovidos += promovidos[i].Total
	}

	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "TOTAL"
	cell = row.AddCell()
	cell.SetInt(totalPromovidos)

	cols = hoja.Cols

	cols[0].Width = 20.0
	cols[1].Width = 15.4

	err = file.Save("ReportePromovidos.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("ReportePromovidos.xlsx")
	if err != nil {
		return "", err
	}
	os.Remove("ReportePromovidos.xlsx")
	return base64.StdEncoding.EncodeToString(data), nil

}

func ReportePromovidosPorEstructuraSeccion(promovidos []Entidades.PromovidosPorEstructuraSeccion) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var cols []*xlsx.Col
	var totalPromovidos int
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Promovidos ")

	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "FECHA"
	cell = row.AddCell()
	t := time.Now().String()
	cell.Value = t[0:10]
	cell = row.AddCell()
	cell.Value = "HORA"
	cell = row.AddCell()
	hora := time.Now()
	cell.Value = strconv.Itoa(hora.Hour()) + ":" + strconv.Itoa(hora.Minute())

	hoja.AddRow()
	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "ESTRUCTURA"
	cell = row.AddCell()
	cell.Value = "SECCION"
	cell = row.AddCell()
	cell.Value = "PROMOVIDOS"

	for i := range promovidos {
		row = hoja.AddRow()
		row.WriteStruct(&promovidos[i], -1)
		totalPromovidos += promovidos[i].Total
	}

	row = hoja.AddRow()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "TOTAL"
	cell = row.AddCell()
	cell.SetInt(totalPromovidos)

	cols = hoja.Cols

	cols[0].Width = 20.0
	cols[1].Width = 12.4
	cols[2].Width = 15.4

	err = file.Save("ReportePromovidos.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("ReportePromovidos.xlsx")
	if err != nil {
		return "", err
	}
	os.Remove("ReportePromovidos.xlsx")
	return base64.StdEncoding.EncodeToString(data), nil

}

func ReporteCasillasIncidencias(incidencias []Entidades.CasillasConIncidencias) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var cols []*xlsx.Col
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Incidencias ")

	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "FECHA"
	cell = row.AddCell()
	t := time.Now().String()
	cell.Value = t[0:10]
	cell = row.AddCell()
	cell.Value = "HORA"
	cell = row.AddCell()
	hora := time.Now()
	cell.Value = strconv.Itoa(hora.Hour()) + ":" + strconv.Itoa(hora.Minute())

	hoja.AddRow()
	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "CASILLA"
	cell = row.AddCell()
	cell.Value = "SECCION"
	cell = row.AddCell()
	cell.Value = "CALLE"
	cell = row.AddCell()
	cell.Value = "COLONIA"
	cell = row.AddCell()
	cell.Value = "CP"
	cell = row.AddCell()
	cell.Value = "INCIDENCIA"

	for i := range incidencias {
		row = hoja.AddRow()
		row.WriteStruct(&incidencias[i], -1)
	}

	cols = hoja.Cols

	cols[0].Width = 7.5
	cols[1].Width = 8
	cols[2].Width = 38
	cols[3].Width = 23
	cols[4].Width = 10
	cols[5].Width = 40

	err = file.Save("ReporteIncidencias.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("ReporteIncidencias.xlsx")
	if err != nil {
		return "", err
	}

	os.Remove("ReporteIncidencias.xlsx")

	return base64.StdEncoding.EncodeToString(data), nil
}

func ReporteMpp(incidencias []Entidades.CoordinadorMppSeccion) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var cols []*xlsx.Col
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Listado ")

	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "FECHA"
	cell = row.AddCell()
	t := time.Now().String()
	cell.Value = t[0:10]
	cell = row.AddCell()
	cell.Value = "HORA"
	cell = row.AddCell()
	hora := time.Now()
	cell.Value = strconv.Itoa(hora.Hour()) + ":" + strconv.Itoa(hora.Minute())

	hoja.AddRow()
	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "COORDINADOR DE MANZANA"
	row.AddCell()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "PROMOTOR"
	row.AddCell()
	row.AddCell()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "PROMOVIDO"

	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "NOMBRE"
	cell = row.AddCell()
	cell.Value = "APP"
	cell = row.AddCell()
	cell.Value = "APM"
	cell = row.AddCell()
	cell.Value = "NOMBRE"
	cell = row.AddCell()
	cell.Value = "APP"
	cell = row.AddCell()
	cell.Value = "APM"
	cell = row.AddCell()
	cell.Value = "CLAVE ELECTOR"
	cell = row.AddCell()
	cell.Value = "NOMBRE"
	cell = row.AddCell()
	cell.Value = "APP"
	cell = row.AddCell()
	cell.Value = "APM"
	cell = row.AddCell()
	cell.Value = "CLAVE ELECTOR"
	cell = row.AddCell()
	cell.Value = "ESTRUCTURA"
	cell = row.AddCell()
	cell.Value = "SECCION"
	cell = row.AddCell()
	cell.Value = "PARTIDO"

	for i := range incidencias {
		row = hoja.AddRow()
		row.WriteStruct(&incidencias[i], -1)
	}

	cols = hoja.Cols

	cols[0].Width = 15.0
	cols[1].Width = 15.0
	cols[2].Width = 15.0
	cols[3].Width = 15.0
	cols[4].Width = 15.0
	cols[5].Width = 15.0
	cols[6].Width = 15.0
	cols[7].Width = 15.0
	cols[8].Width = 15.0
	cols[9].Width = 15.0
	cols[10].Width = 20.0
	cols[11].Width = 13.0
	cols[12].Width = 9.0
	cols[13].Width = 9.0

	err = file.Save("Listado.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("Listado.xlsx")
	if err != nil {
		return "", err
	}

	os.Remove("Listado.xlsx")

	return base64.StdEncoding.EncodeToString(data), nil
}

func ReporteCasillaDireccionHistorial(casilla []Entidades.CasillaDireccionHistorial) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var cols []*xlsx.Col
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Casillas ")

	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "FECHA"
	cell = row.AddCell()
	t := time.Now().String()
	cell.Value = t[0:10]
	cell = row.AddCell()
	cell.Value = "HORA"
	cell = row.AddCell()
	hora := time.Now()
	cell.Value = strconv.Itoa(hora.Hour()) + ":" + strconv.Itoa(hora.Minute())

	hoja.AddRow()
	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "CASILLA"
	cell = row.AddCell()
	cell.Value = "SECCION"
	cell = row.AddCell()
	cell.Value = "CALLE"
	cell = row.AddCell()
	cell.Value = "COLONIA"
	cell = row.AddCell()
	cell.Value = "CP"

	for i := range casilla {
		row = hoja.AddRow()
		row.WriteStruct(&casilla[i], -1)
	}

	cols = hoja.Cols

	cols[0].Width = 7.5
	cols[1].Width = 8
	cols[2].Width = 38
	cols[3].Width = 23
	cols[4].Width = 10

	err = file.Save("ReporteCasillaHistorial.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("ReporteCasillaHistorial.xlsx")
	if err != nil {
		return "", err
	}

	os.Remove("ReporteCasillaHistorial.xlsx")

	return base64.StdEncoding.EncodeToString(data), nil
}

func ReporteFuncionarioCasillaHistorial(funcionario []Entidades.FuncionarioCasillaHistorial) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var cols []*xlsx.Col
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Funcionarios")

	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "FECHA"
	cell = row.AddCell()
	t := time.Now().String()
	cell.Value = t[0:10]
	cell = row.AddCell()
	cell.Value = "HORA"
	cell = row.AddCell()
	hora := time.Now()
	cell.Value = strconv.Itoa(hora.Hour()) + ":" + strconv.Itoa(hora.Minute())

	hoja.AddRow()
	row = hoja.AddRow()
	row.AddCell()
	row.AddCell()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "FUNCIONARIO NUEVO"
	row.AddCell()
	row.AddCell()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "FUNCIONARIO ANTERIOR"

	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "CASILLA"
	cell = row.AddCell()
	cell.Value = "SECCION"
	cell = row.AddCell()
	cell.Value = "ROL"
	cell = row.AddCell()
	cell.Value = "NOMBRE"
	cell = row.AddCell()
	cell.Value = "APP"
	cell = row.AddCell()
	cell.Value = "APM"
	cell = row.AddCell()
	cell.Value = "CLAVE ELECTOR"
	cell = row.AddCell()
	cell.Value = "NOMBRE"
	cell = row.AddCell()
	cell.Value = "APP"
	cell = row.AddCell()
	cell.Value = "APM"
	cell = row.AddCell()
	cell.Value = "CLAVE ELECTOR"

	for i := range funcionario {
		row = hoja.AddRow()
		row.WriteStruct(&funcionario[i], -1)
	}

	cols = hoja.Cols

	cols[0].Width = 7.5
	cols[1].Width = 8
	cols[2].Width = 15
	cols[3].Width = 20
	cols[4].Width = 20
	cols[5].Width = 20
	cols[6].Width = 23
	cols[7].Width = 20
	cols[8].Width = 20
	cols[9].Width = 20
	cols[10].Width = 23

	err = file.Save("ReporteFuncionario.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("ReporteFuncionario.xlsx")
	if err != nil {
		return "", err
	}

	os.Remove("ReporteFuncionario.xlsx")

	return base64.StdEncoding.EncodeToString(data), nil
}

func ReportePromovidosRepetidosEstructuras(promovidos []Entidades.PromovidosRepetidosEstructuras) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var cols []*xlsx.Col
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Promovidos")

	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "FECHA"
	cell = row.AddCell()
	t := time.Now().String()
	cell.Value = t[0:10]
	cell = row.AddCell()
	cell.Value = "HORA"
	cell = row.AddCell()
	hora := time.Now()
	cell.Value = strconv.Itoa(hora.Hour()) + ":" + strconv.Itoa(hora.Minute())

	hoja.AddRow()
	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "PROMOVIDO"
	row.AddCell()
	row.AddCell()
	row.AddCell()
	row.AddCell()
	row.AddCell()
	cell = row.AddCell()
	cell.Value = "PROMOTOR"

	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "NOMBRE"
	cell = row.AddCell()
	cell.Value = "APELLIDO PATERNO"
	cell = row.AddCell()
	cell.Value = "APELLIDO MATERNO"
	cell = row.AddCell()
	cell.Value = "CLAVE ELECTOR"
	cell = row.AddCell()
	cell.Value = "ESTRUCTURA"
	cell = row.AddCell()
	cell.Value = "SECCION"
	cell = row.AddCell()
	cell.Value = "NOMBRE"
	cell = row.AddCell()
	cell.Value = "APELLIDO PATERNO"
	cell = row.AddCell()
	cell.Value = "APELLIDO MATERNO"

	for i := range promovidos {
		row = hoja.AddRow()
		row.WriteStruct(&promovidos[i], -1)
	}

	cols = hoja.Cols

	cols[0].Width = 18
	cols[1].Width = 16
	cols[2].Width = 16
	cols[3].Width = 15
	cols[4].Width = 10
	cols[5].Width = 10
	cols[6].Width = 18
	cols[7].Width = 16
	cols[8].Width = 16

	err = file.Save("ReportePromovidosRepetidos.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("ReportePromovidosRepetidos.xlsx")
	if err != nil {
		return "", err
	}

	os.Remove("ReportePromovidosRepetidos.xlsx")

	return base64.StdEncoding.EncodeToString(data), nil
}

func ReporteCasillaSinRegistrarVotos(casillas []Entidades.CasillasSinregistrarVotos) (string, error) {
	var file *xlsx.File
	var row *xlsx.Row
	var cols []*xlsx.Col
	file = xlsx.NewFile()
	hoja, err := file.AddSheet("Promovidos")

	if err != nil {
		return "", err
	}

	row = hoja.AddRow()
	cell := row.AddCell()
	cell.Value = "FECHA"
	cell = row.AddCell()
	t := time.Now().String()
	cell.Value = t[0:10]
	cell = row.AddCell()
	cell.Value = "HORA"
	cell = row.AddCell()
	hora := time.Now()
	cell.Value = strconv.Itoa(hora.Hour()) + ":" + strconv.Itoa(hora.Minute())

	row = hoja.AddRow()
	cell = row.AddCell()
	cell.Value = "CASILLA"
	cell = row.AddCell()
	cell.Value = "SECCION"
	cell = row.AddCell()
	cell.Value = "LISTA NOMINAL"
	cell = row.AddCell()
	cell.Value = "HORA APERTURA"
	cell = row.AddCell()
	cell.Value = "HORA CIERRE"

	for i := range casillas {
		row = hoja.AddRow()
		row.WriteStruct(&casillas[i], -1)
	}

	cols = hoja.Cols

	cols[0].Width = 15
	cols[1].Width = 16
	cols[2].Width = 15
	cols[3].Width = 20
	cols[4].Width = 20

	err = file.Save("CasillasSinRegistrarVotos.xlsx")
	if err != nil {
		return "", nil
	}

	data, err := ioutil.ReadFile("CasillasSinRegistrarVotos.xlsx")
	if err != nil {
		return "", err
	}

	os.Remove("CasillasSinRegistrarVotos.xlsx")

	return base64.StdEncoding.EncodeToString(data), nil
}
