package MoEstructuras

//Respuesta estructura para contestar a las peticiones hechas por angular
type Respuesta struct {
	Mensaje string
	Data    interface{}
	Codigo  int
}
