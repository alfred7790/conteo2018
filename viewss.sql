--total de coordinadores por seccion y distrito  
create view coord_por_seccion_distrito as
(select prom.distrito,prom.seccion_id,prom.tipo as tipo,prom.total as total ,coordm.tipo as tipoo,coordm.total as totaal from
(select ls.seccion_id,s.distrito as distrito,ls.tipo,count(ls.tipo) as total from lista_coordinadores ls inner join secciones s on ls.seccion_id = s.id where tipo = 'COORDINADOR DE MANZANA' group by ls.seccion_id,ls.tipo,distrito order by seccion_id,distrito) as prom
LEFT JOIN
(select ls.seccion_id,s.distrito as distrito,ls.tipo,count(ls.tipo) as total from lista_coordinadores ls inner join secciones s on ls.seccion_id = s.id where tipo = 'PROMOTOR' group by ls.seccion_id,ls.tipo,distrito order by seccion_id,distrito) as coordm on prom.seccion_id=coordm.seccion_id and prom.distrito = coordm.distrito order by distrito,seccion_id)

--////////// coord por distrito
create view coord_por_distrito as
(select promotor.distrito,promotor.tipo as cargo ,promotor.total as total_promotor,coordmanzana.tipo as cargoo,coordmanzana.total as total_coordmanzana from 
(select distrito,tipo,total from
(select distrito,tipo,sum(total) as total from (select ls.seccion_id,ls.tipo,count(ls.tipo) as total,s.distrito as distrito from lista_coordinadores ls inner join secciones s on ls.seccion_id = s.id group by ls.seccion_id,ls.tipo,distrito) sd GROUP BY distrito,tipo
order by distrito) as d where tipo = 'PROMOTOR') as promotor
left join
(select distrito,tipo,total from
(select distrito,tipo,sum(total) as total from (select ls.seccion_id,ls.tipo,count(ls.tipo) as total,s.distrito as distrito from lista_coordinadores ls inner join secciones s on ls.seccion_id = s.id group by ls.seccion_id,ls.tipo,distrito) sd GROUP BY distrito,tipo
order by distrito) as d where tipo = 'COORDINADOR DE MANZANA') as coordmanzana on promotor.distrito = coordmanzana.distrito)