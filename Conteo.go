package main

import (
	"fmt"
	"net/http"
	_ "os"
	"time"
	"./Modulos/Variables"
	"./Modelos/Entidades"
	"./Controladores/SeccionController"
	"./Controladores/PersonaController"
	"./Controladores/LoginController"
	"./Controladores/CasillaController"
	"./Controladores/PartidoController"
	"./Controladores/CoordinadorController"
	"./Controladores/GrupoController"
	"./Controladores/FuncionarioController"
	"./Controladores/RepresentanteController"
	"./Controladores/PromovidoController"
	"./Controladores/EstructuraController"
	"./Controladores/Reportes"

	"github.com/gin-contrib/static"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

var maincors = cors.Config{
	AllowAllOrigins:  true,
	AllowMethods:     []string{"GET", "POST", "PUT", "OPTIONS"},
	AllowHeaders:     []string{"Authorization", "Accept", "Origin", "Content-Length", "Content-Type"},
	ExposeHeaders:    []string{"Content-Length"},
	AllowCredentials: true,
	MaxAge:           12 * time.Hour,
}

func main() {

	db := Entidades.GetDB()
	defer db.Close()

	app := gin.Default()
	app.Use(cors.New(maincors))
	app.Use(static.Serve("/", static.LocalFile("Public", true)))

	app.NoRoute(func(ctx *gin.Context) {
		ctx.JSON(http.StatusNotFound, gin.H{"error": "Ruta invalida"})
	})

	app.GET("/refreshToken", LoginController.Auth, LoginController.RefreshToken)

	app.POST("/login", LoginController.Login)

	//rutas := app.Group("")

	rutas := app.Group("", LoginController.Auth)
	{
		rutas.GET("/prueba", )
		rutas.GET("/buscarSeccion", SeccionController.BuscarSeccion)
		rutas.POST("/crearSeccion", SeccionController.CrearSeccion)
		rutas.GET("/obtenerSecciones", SeccionController.ObtenerSeccionesFiltradas)

		rutas.GET("/obtenerPersonas", PersonaController.ObtenerPersonas)
		rutas.POST("/crearPersona", PersonaController.CrearPersona)
		rutas.POST("/actualizarPersona", PersonaController.ActualizarPersona)
		rutas.POST("/eliminarTelefono", PersonaController.EliminarTelefono)
		rutas.POST("/agregarCoordinador", PersonaController.AgregarCoordinador)
		rutas.POST("/eliminarCoordinador", PersonaController.ValidarCoordinador)
		rutas.GET("/obtenerCoordinadores", PersonaController.ObtenerCoordinadores)
		rutas.GET("/obtenerRepresentantes", PersonaController.ObtenerRepresentantes)
		rutas.POST("/actualizarCoordinador", PersonaController.ActualizarCoordinador)
		rutas.POST("/agregarRepresentante", PersonaController.AgregarRepresentante)
		rutas.POST("/eliminarRepresentante", PersonaController.EliminarRepresentante)
		rutas.GET("/votoYa", PersonaController.VotoYa)

		rutas.POST("/crearUsuario", PersonaController.CrearUsuario)
		rutas.GET("/obtenerUsuarios", PersonaController.ObtenerUsuarios)

		rutas.POST("/crearPartido", PartidoController.CrearPartido)
		rutas.GET("/obtenerPartidos", PartidoController.ObtenerPartidos)
		rutas.GET("/findPartidos", PartidoController.FindPartidos)

		rutas.POST("/crearGrupo", GrupoController.CrearGrupo)
		rutas.GET("/obtenerGruposFiltrados", GrupoController.ObtenerFiltradosGrupos)

		rutas.GET("/obtenerGrupos", GrupoController.ObtenerGrupos)
		rutas.GET("/obtenerTipos", CoordinadorController.ObtenerRoles)

		rutas.POST("/agregarCoordinadorZona", CoordinadorController.RegistroCoordinadorZona)
		rutas.POST("/eliminarCoordinadorZona", CoordinadorController.EliminarCoordinadorZona)
		rutas.GET("/obtenerCoordinadoresZona", CoordinadorController.ObtenerCoordinadoresZona)
		rutas.POST("/agregarCoordinadorSeccion", CoordinadorController.RegistroCoordinadorSeccion)
		rutas.POST("/eliminarCoordinadorSeccion", CoordinadorController.EliminarCoordinadorSeccion)
		rutas.GET("/obtenerCoordinadoresSeccion", CoordinadorController.ObtenerCoordinadoresSeccion)
		rutas.GET("/obtenerListaCoordinadoresSeccion", CoordinadorController.FindListaCoordinadoresSeccion)
		rutas.POST("/agregarCoordinadorManzana", CoordinadorController.RegistroCoordinadorManzana)
		rutas.POST("/eliminarCoordinadorManzana", CoordinadorController.EliminarCoordinadorManzana)
		rutas.GET("/obtenerListaCoordinadoresManzana", CoordinadorController.FindListaCoordinadoresManzana)
		rutas.POST("/agregarPromotor", CoordinadorController.RegistroPromotor)
		rutas.POST("/eliminarPromotor", CoordinadorController.EliminarPromotor)
		rutas.GET("/obtenerListaPromotores", CoordinadorController.FindListaPromotores)

		rutas.POST("/crearCasilla", CasillaController.CrearCasilla)
		rutas.POST("/eliminarCasilla", CasillaController.EliminarCasilla)
		rutas.GET("/obtenerCasillas", CasillaController.FindCasillas)
		rutas.POST("/actualizarCasilla", CasillaController.ActualizarCasilla)
		rutas.POST("/registrarVotosPartidos", CasillaController.RegistrarVotosxPartidos)
		rutas.POST("/registrarVotosEstimados", CasillaController.RegistrarVotosEstimados)
		rutas.POST("/actualizarHoraCasilla", CasillaController.ActualizarHoraCasilla)
		rutas.POST("/habilitarCasilla", CasillaController.ActualizarDisponibilidad)

		rutas.POST("/generarIncidencias", CasillaController.GenerarIncidencias)
		rutas.POST("/eliminarIncidencia", CasillaController.EliminarIncidencia)
		rutas.POST("/reemplazarDireccion", CasillaController.ReemplazarDireccionCasilla)

		rutas.GET("/obtenerIncidencias", CasillaController.FindIncidencias)
		rutas.GET("/obtenerIncidenciasCasilla", CasillaController.FindIncidenciasCasilla)

		rutas.GET("/obtenerFuncionarios", FuncionarioController.ObtenerFuncionariosFiltrados)
		rutas.POST("/crearFuncionario", FuncionarioController.CrearFuncionario)
		rutas.POST("/eliminarFuncionario", FuncionarioController.EliminarFuncionario)
		rutas.POST("/reemplazarFuncionario", FuncionarioController.ReemplazarFuncionario)

		rutas.POST("/crearRepresentante", RepresentanteController.CrearRepresentante)
		rutas.GET("/obtenerRepresentantesCasilla", RepresentanteController.ObtenerRepresentantesFiltrados)
		rutas.POST("/eliminarRepresentanteCasilla", RepresentanteController.EliminarRepresentante)

		rutas.POST("/registrarPromovido", PromovidoController.RegistroPromovidos)
		rutas.POST("/eliminarPromovido", PromovidoController.EliminarPromovido)
		rutas.GET("/obtenerListaPromovidos", PromovidoController.FindListaPromovidos)

		rutas.POST("/crearEstructura", EstructuraController.InsertarEstructura)
		rutas.GET("/obtenerEstructuras", EstructuraController.ObtenerEstructura)

		rutas.GET("/reporteDistrito", Reportes.CoordxDistrito)
		rutas.GET("/reporteSeccionDistrito", Reportes.CoordxDistritoSeccion)
		rutas.GET("/reporteTotalDistrito", Reportes.TotalxDistrito)
		rutas.GET("/reporteTotalDistritoSeccion", Reportes.TotalxDistritoSeccion)
		rutas.GET("/reportePromovidosRepetidos", Reportes.PromovidosRepetidos)
		rutas.GET("/concentradoTotal", Reportes.ConcentradoTotal)
		rutas.GET("/obtenerConcentradoPorEstructura", Reportes.ObtenerPromovidosPorEstructura)
		rutas.GET("/obtenerTotalPromovidos", Reportes.ObtenerTotalPromovidos)

		rutas.GET("/votosPartido", Reportes.VotosTotalPartido)
		rutas.GET("/votosCandidato", Reportes.VotosTotalCandidato)
		rutas.GET("/reporteVotosPartido", Reportes.ReporteVotosTotalPartido)
		rutas.GET("/reportePromovidosPorEstructura", Reportes.ReportePromovidosPorEstructura)
		rutas.GET("/reportePromovidosPorEstructuraSeccion", Reportes.ReportePromovidosPorEstructuraSeccion)
		rutas.GET("/reporteCasillaIncidencia", Reportes.ReporteCasillasIncidencias)
		rutas.GET("/reporteMpp", Reportes.ReporteMpp)
		rutas.GET("/reporteCasillaDireccionHistorial", Reportes.ReporteCasillaDireccionHistorial)
		rutas.GET("/reporteFuncionarioCasilla", Reportes.ReporteFuncionarioCasillaHistorial)
		rutas.GET("/reportePromovidosRepetidosEstructura", Reportes.PromovidosRepetidosEstructura)
		rutas.GET("/reporteCasillasSinRegistro", Reportes.CasillasSinRegistrarVotos)

		rutas.GET("/vistaEstadoCasillas", Reportes.ObtenerEstadoCasillas)
		rutas.GET("/vistaEstadoCasillasCerradas", Reportes.ObtenerEstadoCasillasCerradas)
		rutas.GET("/vistaVotosCasilla", Reportes.ObtenerVotosCasilla)
		rutas.GET("/vistaVotosPartidoSeccion", Reportes.BuscarSeccionPorVotosPartido)
		rutas.GET("/vistaFlujoVotos",Reportes.ObtenerFlujoVotosEstimados)
		rutas.GET("/obtenerColonias",Reportes.ObtenerColonias)
		rutas.GET("/obtenerColoniasFiltradas",Reportes.FindColonias)
		rutas.GET("/vistaColoniaVotos",Reportes.ObtenerColoniasPartidosVotos)
	}

	var DataCfg = MoVar.CargaSeccionCFG(MoVar.SecDefault)
	if DataCfg.Puerto != "" {
		fmt.Println("Ejecutandose en el puerto: ", DataCfg.Puerto)
		fmt.Println("Acceder a la siguiente url: ", DataCfg.BaseURL)
		app.Run(":" + DataCfg.Puerto)
	} else {
		fmt.Println("No ha especificado un puerto para ejecutar la App, verifique su archivo CFG e intente de nuevo.")
		return
	}

}
